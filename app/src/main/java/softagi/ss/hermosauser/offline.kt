package softagi.ss.hermosauser

import android.app.Application
import com.google.firebase.database.FirebaseDatabase

class offline:Application()
{
    override fun onCreate() {
        super.onCreate()

        FirebaseDatabase.getInstance().setPersistenceEnabled(true)
    }
}