package softagi.ss.hermosauser.utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson

lateinit var sharedPreferences: SharedPreferences
private val gson = Gson()

fun initSharedPreferences(context: Context)
{
    sharedPreferences = context.getSharedPreferences("HERMOSA_PREFERENCES", 0)
}

fun setToken(token:String)
{
    savePrimitive("token", token)
}

fun getToken():String
{
    return getPrimitive("token", "") as String
}

fun setBaseUrl(url:String)
{
    savePrimitive("baseUrl", url)
}

fun getBaseUrl():String
{
    return getPrimitive("baseUrl", "") as String
}

fun setHasAuth()
{
    savePrimitive("hasAuth", true)
}

fun getHasAuth():Boolean
{
    return getPrimitive("hasAuth", false) as Boolean
}

fun savePrimitive(key: String?, value: Any?)
{
    val editor = sharedPreferences.edit()
    if (value is String) editor.putString(key, value as String?)
    if (value is Boolean) editor.putBoolean(key, (value as Boolean?)!!)
    if (value is Float) editor.putFloat(key, (value as Float?)!!)
    if (value is Int) editor.putInt(key, (value as Int?)!!)
    if (value is Long) editor.putLong(key, (value as Long?)!!)
    editor.apply()
}

fun getPrimitive(key: String?, defVal: Any?): Any?
{
    if (defVal is String) return sharedPreferences.getString(key, defVal as String?)
    if (defVal is Boolean) return sharedPreferences.getBoolean(key, (defVal as Boolean?)!!)
    if (defVal is Float) return sharedPreferences.getFloat(key, (defVal as Float?)!!)
    if (defVal is Int) return sharedPreferences.getInt(key, (defVal as Int?)!!)
    return if (defVal is Long) sharedPreferences.getLong(key, (defVal as Long?)!!)
    else null
}

fun saveModel(key: String?, model: Any?)
{
    val s = gson.toJson(model)
    //Log.d(mbn.mans.d2020.custom.BaseSharedPreferences.TAG, "saveModel: $s")
    sharedPreferences.edit()
        .putString(key, s)
        .apply()
}

fun <T> getModel(key: String?, model: Class<T>?): T
{
    val s = sharedPreferences.getString(key, "")
    Log.d("hello", "getModel: $s")
    return gson.fromJson(s, model)
}