package softagi.ss.hermosauser.utils

import android.app.Activity
import android.content.res.Configuration
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import softagi.ss.hermosauser.R
import java.util.*

var isHome : Boolean = false

fun navigateTo(fragment: Fragment, to:Fragment, backStack:Boolean)
{
    if (backStack)
    {
        fragment
            .activity!!
            .supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
            .replace(R.id.container, to)
            .addToBackStack("fragment")
            .commit()
    } else
    {
        fragment
            .activity!!
            .supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right)
            .replace(R.id.container, to)
            .disallowAddToBackStack()
            .commit()
    }
}

fun getLanguage() : String
{
    return Locale.getDefault().language
}

fun setArabic(activity : Activity)
{
    val locale = Locale("ar")
    Locale.setDefault(locale)
    // Create a new configuration object
    val config = Configuration()
    // Set the locale of the new configuration
    config.locale = locale
    // Update the configuration of the Accplication context
    activity.resources.updateConfiguration(
        config,
        activity.resources.displayMetrics)
}