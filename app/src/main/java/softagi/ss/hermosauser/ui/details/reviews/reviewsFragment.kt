package softagi.ss.hermosauser.ui.details.reviews

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import company.tap.gosellapi.GoSellSDK
import company.tap.gosellapi.internal.api.callbacks.GoSellError
import company.tap.gosellapi.internal.api.models.Authorize
import company.tap.gosellapi.internal.api.models.Charge
import company.tap.gosellapi.internal.api.models.PhoneNumber
import company.tap.gosellapi.internal.api.models.Token
import company.tap.gosellapi.open.controllers.SDKSession
import company.tap.gosellapi.open.controllers.ThemeObject
import company.tap.gosellapi.open.delegate.SessionDelegate
import company.tap.gosellapi.open.enums.AppearanceMode
import company.tap.gosellapi.open.enums.CardType
import company.tap.gosellapi.open.enums.TransactionMode
import company.tap.gosellapi.open.models.CardsList
import company.tap.gosellapi.open.models.Customer
import company.tap.gosellapi.open.models.Customer.CustomerBuilder
import company.tap.gosellapi.open.models.Receipt
import company.tap.gosellapi.open.models.TapCurrency
import kotlinx.android.synthetic.main.reviews_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.salonModel
import java.math.BigDecimal

class reviewsFragment(var uId: salonModel?) :Fragment(R.layout.reviews_fragment), SessionDelegate
{
    private val SDK_REQUEST_CODE = 1001
    private var sdkSession: SDKSession? = null

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        startSDK()

        ok_btn.setOnClickListener {
            //sdkSession!!.setButtonView(ok_btn, activity, SDK_REQUEST_CODE)
            sdkSession!!.start(activity)
            Log.d("TAP", "checkout clicked")
        }
    }

    private fun startSDK()
    {
        /**
         * Required step.
         * Configure SDK with your Secret API key and App Bundle name registered with tap company.
         */
        configureApp()
        /**
         * Optional step
         * Here you can configure your app theme (Look and Feel).
         */
        configureSDKThemeObject()
        /**
         * Required step.
         * Configure SDK Session with all required data.
         */
        configureSDKSession()
        /**
         * Required step.
         * Choose between different SDK modes
         */
        configureSDKMode()
        /**
         * If you included Tap Pay Button then configure it first, if not then ignore this step.
         */
        //initPayButton()
    }

    private fun configureApp()
    {
        GoSellSDK.init(
            requireActivity(),
            "sk_test_3gtGQK9A5NaBCpDzIeJSurvF",
            "softagi.ss.hermosauser"
        ) // to be replaced by merchant
        GoSellSDK.setLocale("en") //  language to be set by merchant
    }

    private fun configureSDKThemeObject()
    {
        ThemeObject.getInstance()
            .setAppearanceMode(AppearanceMode.WINDOWED_MODE)
            .setSdkLanguage("ar")
            .setHeaderFont(ResourcesCompat.getFont(requireContext(), R.font.hacen))
            .setHeaderTextColor(resources.getColor(R.color.black1))
            .setHeaderTextSize(17)
            .setHeaderBackgroundColor(resources.getColor(R.color.french_gray_new))
            .setCardInputFont(ResourcesCompat.getFont(requireContext(), R.font.hacen))
            .setCardInputTextColor(resources.getColor(R.color.black))

            .setCardInputDescriptionFont(ResourcesCompat.getFont(requireContext(), R.font.hacen))
            .setCardInputFont(ResourcesCompat.getFont(requireContext(), R.font.hacen))
            .setHeaderFont(ResourcesCompat.getFont(requireContext(), R.font.hacen))
            .setPayButtonFont(ResourcesCompat.getFont(requireContext(), R.font.hacen))

            .setCardInputInvalidTextColor(resources.getColor(R.color.red))
            .setCardInputPlaceholderTextColor(resources.getColor(R.color.gray))
            .setSaveCardSwitchOffThumbTint(resources.getColor(R.color.french_gray_new))
            .setSaveCardSwitchOnThumbTint(resources.getColor(R.color.colorPrimaryDark))
            .setSaveCardSwitchOffTrackTint(resources.getColor(R.color.french_gray))
            .setSaveCardSwitchOnTrackTint(resources.getColor(R.color.trackOff))
            .setScanIconDrawable(resources.getDrawable(R.drawable.btn_card_scanner_normal))
            .setPayButtonResourceId(R.drawable.btn_pay_selector) //btn_pay_merchant_selector
            .setPayButtonFont(ResourcesCompat.getFont(requireContext(), R.font.hacen))
            .setPayButtonDisabledTitleColor(resources.getColor(R.color.white))
            .setPayButtonEnabledTitleColor(resources.getColor(R.color.white))
            .setPayButtonEnabledBackgroundColor(resources.getColor(R.color.colorAccent))
            .setPayButtonCornerRadius(15)
            .setPayButtonTextSize(14)
            .setPayButtonLoaderVisible(true)
            .setPayButtonSecurityIconVisible(false) // setup dialog textcolor and textsize
            .setDialogTextColor(resources.getColor(R.color.black1)).dialogTextSize =
            17 // **Optional**
    }

    private fun getCustomer(): Customer?
    {
        return CustomerBuilder(null).email("abc@abc.com").firstName("firstname")
            .lastName("lastname").metadata("").phone(PhoneNumber("20", "1115342559"))
            .middleName("middlename").build()
    }

    private fun configureSDKSession()
    {
        // Instantiate SDK Session
        if (sdkSession == null) sdkSession = SDKSession() //** Required **

        // pass your activity as a session delegate to listen to SDK internal payment process follow
        sdkSession!!.addSessionDelegate(this) //** Required **

        // initiate PaymentDataSource
        sdkSession!!.instantiatePaymentDataSource() //** Required **

        // set transaction currency associated to your account
        sdkSession!!.setTransactionCurrency(TapCurrency("KWD")) //** Required **

        // Using static CustomerBuilder method available inside TAP Customer Class you can populate TAP Customer object and pass it to SDK
        sdkSession!!.setCustomer(getCustomer()) //** Required **

        // Set Total Amount. The Total amount will be recalculated according to provided Taxes and Shipping
        sdkSession!!.setAmount(BigDecimal(1)) //** Required **

        // Set Payment Items array list
        sdkSession!!.setPaymentItems(ArrayList()) // ** Optional ** you can pass empty array list

       sdkSession!!.setPaymentType("CARD") //** Merchant can pass paymentType

        // Set Taxes array list
        sdkSession!!.setTaxes(ArrayList()) // ** Optional ** you can pass empty array list

        // Set Shipping array list
        sdkSession!!.setShipping(ArrayList()) // ** Optional ** you can pass empty array list

        // Post URL
        sdkSession!!.setPostURL("") // ** Optional **

        // Payment Description
        sdkSession!!.setPaymentDescription("") //** Optional **

        // Payment Extra Info
        sdkSession!!.setPaymentMetadata(HashMap()) // ** Optional ** you can pass empty array hash map

        // Payment Reference
        sdkSession!!.setPaymentReference(null) // ** Optional ** you can pass null

        // Payment Statement Descriptor
        sdkSession!!.setPaymentStatementDescriptor("") // ** Optional **

        sdkSession!!.setCardInfo("512350000000008","05","21","100","Mans",null)

        // Enable or Disable Saving Card
        sdkSession!!.isUserAllowedToSaveCard(true) //  ** Required ** you can pass boolean

        // Enable or Disable 3DSecure
        sdkSession!!.isRequires3DSecure(true)

        //Set Receipt Settings [SMS - Email ]
        sdkSession!!.setReceiptSettings(
            Receipt(
                false,
                true
            )
        ) // ** Optional ** you can pass Receipt object or null

        // Set Authorize Action
        sdkSession!!.setAuthorizeAction(null) // ** Optional ** you can pass AuthorizeAction object or null
        sdkSession!!.setDestination(null) // ** Optional ** you can pass Destinations object or null
        sdkSession!!.setMerchantID(null) // ** Optional ** you can pass merchant id or null

          sdkSession!!.setCardType(CardType.CREDIT) // ** Optional ** you can pass which cardType[CREDIT/DEBIT] you want.By default it loads all available cards for Merchant.
    }

    private fun configureSDKMode()
    {
        /**
         * You have to choose only one Mode of the following modes:
         * Note:-
         * - In case of using PayButton, then don't call sdkSession.start(this) because the SDK will start when user clicks the tap pay button.
         */
        //////////////////////////////////////////////////////    SDK with UI //////////////////////
        /**
         * 1- Start using  SDK features through SDK main activity (With Tap CARD FORM)
         */
        startSDKWithUI()

        //////////////////////////////////////////////////////    SDK Tokenization without UI //////////////////////
        /**
         * 2- Start using  SDK to tokenize your card without using SDK main activity (Without Tap CARD FORM)
         * After the SDK finishes card tokenization, it will notify this activity with tokenization result in either
         * cardTokenizedSuccessfully(@NonNull String token) or sdkError(@Nullable GoSellError goSellError)
         */
        //startSDKTokenizationWithoutUI()

        //////////////////////////////////////////////////////    SDK Saving card without UI //////////////////////
        /**
         * 3- Start using  SDK to save your card without using SDK main activity ((Without Tap CARD FORM))
         * After the SDK finishes card tokenization, it will notify this activity with save card result in either
         * cardSaved(@NonNull Charge charge) or sdkError(@Nullable GoSellError goSellError)
         *
         */
//         startSDKSavingCardWithoutUI()
    }

    private fun startSDKTokenizationWithoutUI()
    {
        if (sdkSession != null)
        {
            // set transaction mode [ TransactionMode.TOKENIZE_CARD_NO_UI ]
            sdkSession!!.transactionMode = TransactionMode.TOKENIZE_CARD_NO_UI //** Required **
            // pass card info to SDK
            sdkSession!!.setCardInfo(
                "5123450000000008",
                "05",
                "21",
                "100",
                "Haitham Elsheshtawy",
                null
            ) //** Required **
            // if you are not using tap button then start SDK using the following call
            //sdkSession!!.start(activity)
        }
    }

    private fun startSDKWithUI()
    {
        if (sdkSession != null)
        {
            // set transaction mode [TransactionMode.PURCHASE - TransactionMode.AUTHORIZE_CAPTURE - TransactionMode.SAVE_CARD - TransactionMode.TOKENIZE_CARD ]
            sdkSession!!.transactionMode = TransactionMode.PURCHASE //** Required **
            // if you are not using tap button then start SDK using the following call
            //sdkSession.start(this)
        }
    }

    private fun initPayButton()
    {
        //payButtonView = findViewById(R.id.payButtonId)
        if (ThemeObject.getInstance().payButtonFont != null) payButtonId!!.setupFontTypeFace(
            ThemeObject.getInstance().payButtonFont
        )
        if (ThemeObject.getInstance()
                .payButtonDisabledTitleColor != 0 && ThemeObject.getInstance()
                .payButtonEnabledTitleColor != 0
        ) payButtonId!!.setupTextColor(
            ThemeObject.getInstance().payButtonEnabledTitleColor,
            ThemeObject.getInstance().payButtonDisabledTitleColor
        )
        if (ThemeObject.getInstance().payButtonTextSize != 0) payButtonId!!.payButton.textSize =
            ThemeObject.getInstance().payButtonTextSize.toFloat()
        //
        if (ThemeObject.getInstance()
                .isPayButtSecurityIconVisible
        ) payButtonId!!.securityIconView.visibility = if (ThemeObject.getInstance()
                .isPayButtSecurityIconVisible
        ) View.VISIBLE else View.INVISIBLE
        if (ThemeObject.getInstance()
                .payButtonResourceId != 0
        ) payButtonId!!.setBackgroundSelector(ThemeObject.getInstance().payButtonResourceId)
        if (sdkSession != null)
        {
            val trx_mode =
                sdkSession!!.transactionMode
            if (trx_mode != null)
            {
                if (TransactionMode.SAVE_CARD == trx_mode || TransactionMode.SAVE_CARD_NO_UI == trx_mode)
                {
                    payButtonId!!.payButton.text =
                        getString(company.tap.gosellapi.R.string.save_card)
                } else if (TransactionMode.TOKENIZE_CARD == trx_mode || TransactionMode.TOKENIZE_CARD_NO_UI == trx_mode)
                {
                    payButtonId!!.payButton.text =
                        getString(company.tap.gosellapi.R.string.tokenize)
                } else
                {
                    payButtonId!!.payButton.text = getString(company.tap.gosellapi.R.string.pay)
                }
            } else
            {
                configureSDKMode()
            }
            sdkSession!!.setButtonView(payButtonId, activity, SDK_REQUEST_CODE)
        }
    }

    override fun sessionCancelled()
    {
        Log.d("TAP", "cancel")
    }

    override fun savedCardsList(cardsList: CardsList)
    {

    }

    override fun sessionIsStarting()
    {
        Log.d("TAP", "done")
    }

    override fun invalidCardDetails() {

    }

    override fun cardSavingFailed(charge: Charge) {

    }

    override fun backendUnknownError(message: String?) {

    }

    override fun userEnabledSaveCardOption(saveCardEnabled: Boolean) {

    }

    override fun cardSaved(charge: Charge) {

    }

    override fun paymentSucceed(charge: Charge)
    {
        val c = Gson().toJson(charge)
        Log.d("TAP", c)
        Toast.makeText(context, "transaction successfully", Toast.LENGTH_LONG).show()

        Log.d("TAP", charge.status.toString())
        Log.d("TAP", charge.description)
        Log.d("TAP", charge.response.message)

        if(charge.card !=null)
        {
            Log.d("TAP", charge.card!!.firstSix)
            Log.d("TAP", charge.card!!.last4)
            Log.d("TAP", charge.card!!.getObject())
            Log.d("TAP", charge.card!!.brand)
        }

        if(charge.acquirer !=null)
        {
            Log.d("TAP", charge.acquirer!!.id)
            Log.d("TAP", charge.acquirer!!.response.code)
            Log.d("TAP", charge.acquirer!!.response.message)
        }

        if(charge.source !=null)
        {
            Log.d("TAP", charge.source.id)
            Log.d("TAP", charge.source.channel.toString())
            Log.d("TAP", charge.source.getObject()!!)
            Log.d("TAP", charge.source.paymentMethodStringValue)
            Log.d("TAP", charge.source.paymentType!!)
            Log.d("TAP", charge.source.type.toString())
        }

        if(charge.expiry !=null)
        {
            Log.d("TAP", charge.expiry!!.type)
            Log.d("TAP", charge.expiry!!.period.toString())
        }
    }

    override fun authorizationFailed(authorize: Authorize?)
    {
        Log.d("TAP", authorize!!.id)
    }

    override fun cardTokenizedSuccessfully(token: Token)
    {

    }

    override fun authorizationSucceed(authorize: Authorize)
    {

    }

    override fun invalidTransactionMode() {

    }

    override fun sdkError(goSellError: GoSellError?)
    {
        Log.d("TAP", goSellError.toString())
    }

    override fun sessionFailedToStart()
    {
        Log.d("TAP", "failed")
    }

    override fun paymentFailed(charge: Charge?)
    {
        Log.d("TAP", charge!!.id)
    }

    override fun sessionHasStarted()
    {
        Log.d("TAP","has started")
    }

    override fun invalidCustomerID() {

    }
}