package softagi.ss.hermosauser.ui.splash

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.ui.login.LoginActivity
import softagi.ss.hermosauser.ui.main.MainActivity
import java.util.*

class SplashActivity : AppCompatActivity()
{
    var firebaseUser:FirebaseUser?= null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        timer()
        fullScreen()
    }

    private fun fullScreen()
    {
        val flags = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        window.decorView.systemUiVisibility = flags
        val decorView = window.decorView
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0)
            {
                decorView.systemUiVisibility = flags
            }
        }
    }

    private fun timer()
    {
        firebaseUser = FirebaseAuth.getInstance().currentUser

        val timerTask: TimerTask = object : TimerTask()
        {
            override fun run()
            {
                if (firebaseUser != null)
                {
                    //FirebaseDatabase.getInstance().reference.child("users").
                    startActivity(Intent(applicationContext, MainActivity::class.java))
                    finish()
                } else
                {
                    startActivity(Intent(applicationContext, LoginActivity::class.java))
                    finish()
                }
            }
        }

        Timer().schedule(timerTask, 3000)
    }
}