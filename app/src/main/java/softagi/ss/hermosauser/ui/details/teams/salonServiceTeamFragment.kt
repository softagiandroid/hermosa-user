package softagi.ss.hermosauser.ui.details.teams

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.salon_service_team_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.*
import softagi.ss.hermosauser.ui.cart.cartFragment
import softagi.ss.hermosauser.ui.main.MainActivity
import softagi.ss.hermosauser.ui.onNavigate
import softagi.ss.hermosauser.utils.isHome
import softagi.ss.hermosauser.utils.navigateTo
import java.util.*

class salonServiceTeamFragment(
    var salonModel: salonModel,
    var serviceModel: serviceModel
) :Fragment(R.layout.salon_service_team_fragment)
{
    var onNavigate: onNavigate? = null

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        onNavigate = context as onNavigate
        onNavigate!!.onNavigate("salon_service")
    }

    override fun onResume()
    {
        super.onResume()
        onNavigate!!.onNavigate("salon_service")
    }

    override fun onPause()
    {
        super.onPause()
        isHome = false
    }

    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var list:MutableList<teamModel> = mutableListOf()
    private var progressDialog: ProgressDialog? = null
    private var loginPrefsEditor: SharedPreferences.Editor? = null
    private var loginPreferences:SharedPreferences?= null

    private var savedSalonId:String?= null

    companion object
    {
        var addSalon: Button? = null
        var addHome: Button? = null
        var salonPrice: TextView? = null
        var homePrice: TextView? = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        initPref()
        initFirebase()
        initProgress()
        getData(salonModel.uId, serviceModel.id)
        cartFab()
    }

    private fun cartFab()
    {
        cart_fab.setOnClickListener {
            navigateTo(this, cartFragment(), true)
        }
    }

    @SuppressLint("CommitPrefEdits")
    private fun initPref()
    {
        loginPreferences = activity!!.getSharedPreferences("loginPrefs", Context.MODE_PRIVATE)
        loginPrefsEditor = loginPreferences!!.edit()
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    private fun getData(salonId: String?, serviceId: String?)
    {
        databaseReference!!.child("accept_salons_teams").child(salonId!!).child(serviceId!!).addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                list.clear()

                for (dataSnapshot1 in p0.children)
                {
                    val teamModel = dataSnapshot1.getValue(teamModel::class.java)
                    list.add(teamModel!!)
                }

                teams_recycler.adapter  = teamsAdapter(activity!!, list, context!!, salonModel, serviceModel, databaseReference!!, progressDialog, loginPrefsEditor, savedSalonId, loginPreferences)

                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun initFirebase()
    {
        teams_recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val dividerItemDecoration = DividerItemDecoration(context, RecyclerView.VERTICAL)
        teams_recycler.addItemDecoration(dividerItemDecoration)

        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }

    internal class teamsAdapter(
        var activity: FragmentActivity,
        var teamModel: MutableList<teamModel>,
        var context: Context,
        var salonModel: salonModel,
        var serviceModel: serviceModel,
        var databaseReference: DatabaseReference,
        var progressDialog: ProgressDialog?,
        var loginPrefsEditor: SharedPreferences.Editor?,
        var savedSalonId: String?,
        var loginPreferences: SharedPreferences?
    ) : RecyclerView.Adapter<teamsAdapter.teamsVH>()
    {
        var team:teamModel? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): teamsVH
        {
            val view = LayoutInflater.from(context).inflate(R.layout.team_item, parent, false)
            return teamsVH(view)
        }

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: teamsVH, position: Int)
        {
            team = teamModel[position]
            val image = team!!.imageUrl
            val titleA = team!!.Namear
            val descA = team!!.descAr
            val service = team!!.serviceTitle

            holder.title.text = titleA
            holder.service.text = service
            holder.desc.text = descA

            if (image != null && image.isNotEmpty())
            {
                Picasso.get()
                    .load(image)
                    .placeholder(R.drawable.logo2)
                    .error(R.drawable.logo2)
                    .into(holder.image)
            } else
            {
                holder.image.setImageResource(R.drawable.hijab)
            }

            holder.addToCart.setOnClickListener {
                if (isHome)
                {
                    cartDialog()
                } else
                {
                    progressDialog!!.show()
                    savedSalonId = loginPreferences!!.getString("salon_id", "0")

                    if (savedSalonId == salonModel.uId || savedSalonId == "0")
                    {
                        addToCart(salonModel, serviceModel, team!!)
                    } else
                    {
                        progressDialog!!.dismiss()
                        Toast.makeText(context, activity.resources.getString(R.string.cartError), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }

        private fun addToCart(salonModel: salonModel, serviceModel: serviceModel, teamModel: teamModel)
        {
            val uid = FirebaseAuth.getInstance().uid
            databaseReference.child("users").child(uid!!).addListenerForSingleValueEvent(object : ValueEventListener
            {
                override fun onCancelled(p0: DatabaseError)
                {

                }

                override fun onDataChange(p0: DataSnapshot)
                {
                    val userModel = p0.getValue(userModel::class.java)

                    if (isHome)
                    {
                        val key = databaseReference.child("cart").child("home").child(uid).push().key

                        val cart = cartModel(
                            userModel,
                            salonModel,
                            serviceModel,
                            teamModel,
                            key
                        )

                        val bookingValue = cart.toMap()

                        val childUpdates = HashMap<String, Any>()
                        childUpdates["/cart/home/$uid/$key"] = bookingValue

                        //reference!!.child("pending_salons").child(uid).setValue(user)
                        databaseReference.updateChildren(childUpdates).addOnCompleteListener {
                            if (it.isSuccessful)
                            {
                                Toast.makeText(context, activity.resources.getString(R.string.added_to_cart), Toast.LENGTH_SHORT).show()
                                progressDialog!!.dismiss()

                                loginPrefsEditor!!.putString("salon_id", salonModel.uId)
                                loginPrefsEditor!!.apply()

//                            val intent = Intent(context, MainActivity::class.java)
//                            activity.startActivity(intent)
//                            activity.finish()
                            }
                        }
                    } else
                    {
                        val key = databaseReference.child("cart").child("salons").child(uid).push().key

                        val cart = cartModel(
                            userModel,
                            salonModel,
                            serviceModel,
                            teamModel,
                            key
                        )

                        val bookingValue = cart.toMap()

                        val childUpdates = HashMap<String, Any>()
                        childUpdates["/cart/salons/$uid/$key"] = bookingValue

                        //reference!!.child("pending_salons").child(uid).setValue(user)
                        databaseReference.updateChildren(childUpdates).addOnCompleteListener {
                            if (it.isSuccessful)
                            {
                                Toast.makeText(context, activity.resources.getString(R.string.added_to_cart), Toast.LENGTH_SHORT).show()
                                progressDialog!!.dismiss()

                                loginPrefsEditor!!.putString("salon_id", salonModel.uId)
                                loginPrefsEditor!!.apply()

//                            val intent = Intent(context, MainActivity::class.java)
//                            activity.startActivity(intent)
//                            activity.finish()
                            }
                        }
                    }
                }
            })
        }

        override fun getItemCount(): Int
        {
            return teamModel.size
        }

        private fun cartDialog()
        {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
            dialog.setContentView(R.layout.home_dialog)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(true)
            val lp = WindowManager.LayoutParams()
            lp.copyFrom(dialog.window!!.attributes)
            lp.width = WindowManager.LayoutParams.MATCH_PARENT
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT

            addSalon = dialog.findViewById(R.id.salon_cart) as Button
            addHome = dialog.findViewById(R.id.home_cart) as Button
            salonPrice = dialog.findViewById(R.id.salon_text) as TextView
            homePrice = dialog.findViewById(R.id.home_text) as TextView

            salonPrice!!.append(" " + serviceModel.price + " " + serviceModel.currency)
            homePrice!!.append(" " + serviceModel.homePrice + " " + serviceModel.currency)

            addSalon!!.setOnClickListener {
                progressDialog!!.show()
                savedSalonId = loginPreferences!!.getString("salon_id", "0")

                if (savedSalonId == salonModel.uId || savedSalonId == "0")
                {
                    val uid = FirebaseAuth.getInstance().uid
                    databaseReference.child("users").child(uid!!).addListenerForSingleValueEvent(object : ValueEventListener
                    {
                        override fun onCancelled(p0: DatabaseError)
                        {

                        }

                        override fun onDataChange(p0: DataSnapshot)
                        {
                            val userModel = p0.getValue(userModel::class.java)

                            val key = databaseReference.child("cart").child("salons").child(uid).push().key

                            val cart = cartModel(
                                userModel,
                                salonModel,
                                serviceModel,
                                team,
                                key
                            )

                            val bookingValue = cart.toMap()

                            val childUpdates = HashMap<String, Any>()
                            childUpdates["/cart/salons/$uid/$key"] = bookingValue

                            //reference!!.child("pending_salons").child(uid).setValue(user)
                            databaseReference.updateChildren(childUpdates).addOnCompleteListener {
                                if (it.isSuccessful)
                                {
                                    Toast.makeText(context, activity.resources.getString(R.string.added_to_cart), Toast.LENGTH_SHORT).show()
                                    progressDialog!!.dismiss()

                                    loginPrefsEditor!!.putString("salon_id", salonModel.uId)
                                    loginPrefsEditor!!.apply()

                                    dialog.dismiss()
                                }
                            }
                        }
                    })
                } else
                {
                    progressDialog!!.dismiss()
                    Toast.makeText(context, activity.resources.getString(R.string.cartError), Toast.LENGTH_SHORT).show()
                }
            }

            addHome!!.setOnClickListener {
                progressDialog!!.show()
                savedSalonId = loginPreferences!!.getString("salon_id", "0")

                if (savedSalonId == salonModel.uId || savedSalonId == "0")
                {
                    addToCart(salonModel, serviceModel, team!!)
                    dialog.dismiss()
                } else
                {
                    progressDialog!!.dismiss()
                    Toast.makeText(context, activity.resources.getString(R.string.cartError), Toast.LENGTH_SHORT).show()
                }
            }

            dialog.show()
            dialog.window!!.attributes = lp
        }

        internal inner class teamsVH(itemView: View) : RecyclerView.ViewHolder(itemView)
        {
            var title: TextView = itemView.findViewById(R.id.team_name)
            var service: TextView = itemView.findViewById(R.id.team_service)
            var desc: TextView = itemView.findViewById(R.id.team_desc)
            var addToCart: TextView = itemView.findViewById(R.id.add_to_cart)
            var image: ImageView = itemView.findViewById(R.id.team_image)
        }
    }
}