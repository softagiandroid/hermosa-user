package softagi.ss.hermosauser.ui.home

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.smarteist.autoimageslider.SliderViewAdapter
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.home_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.mainServiceModel
import softagi.ss.hermosauser.models.salonModel
import softagi.ss.hermosauser.models.sliderModel
import softagi.ss.hermosauser.ui.chat.ChatActivity
import softagi.ss.hermosauser.ui.details.SalonDetailsActivity

class homeFragment:Fragment(R.layout.home_fragment)
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var list:MutableList<salonModel> = mutableListOf()
    private var slider:MutableList<sliderModel> = mutableListOf()
    private var mainServices:MutableList<mainServiceModel> = mutableListOf()
    private var progressDialog: ProgressDialog? = null

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        setHasOptionsMenu(true)
        initToolbar()
        initFirebase()
        initProgress()
        getData()
        getMainServices()
        getSlider()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.home_menu, menu)
    }

    private fun getMainServices()
    {
        databaseReference!!.child("main_services").addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                mainServices.clear()

                for (dataSnapshot1 in p0.children)
                {
                    val mainServiceModel = dataSnapshot1.getValue(mainServiceModel::class.java)
                    mainServices.add(mainServiceModel!!)
                }

                services_recycler.adapter  = mainServicesAdapter(activity!!, mainServices, context!!)

                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun initToolbar()
    {
        images_recycler.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        salons_recycler.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        services_recycler.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).title = resources.getString(R.string.home)
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    private fun getData()
    {
        databaseReference!!.child("accepted_salons").addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                list.clear()

                for (dataSnapshot1 in p0.children)
                {
                    val salonModel = dataSnapshot1.getValue(salonModel::class.java)
                    list.add(salonModel!!)
                }

                salons_recycler.adapter  = salonsAdapter(activity!!, list, context!!)

                newest_txt.visibility = View.VISIBLE
                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun initFirebase()
    {
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }

    internal class salonsAdapter(var activity: FragmentActivity, var salonModels: MutableList<salonModel>, var context: Context) : RecyclerView.Adapter<salonsAdapter.salonsVH>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): salonsVH
        {
            val view = LayoutInflater.from(context).inflate(R.layout.home_salon_item, parent, false)
            return salonsVH(view)
        }

        override fun onBindViewHolder(holder: salonsVH, position: Int)
        {
            val salonModel = salonModels[position]
            val title = salonModel.salonName
            val image = salonModel.salonImage
            val visa = salonModel.visa
            val id = salonModel.uId

            holder.salonTitle.text = title

            if (visa!!)
            {
                holder.salonVisa.visibility = View.VISIBLE
            }

            if (image!!.isNotEmpty())
            {
                Picasso.get()
                    .load(image)
                    .placeholder(R.drawable.logo2)
                    .error(R.drawable.logo2)
                    .into(holder.salonImage)
            }

            holder.itemView.setOnClickListener {
                val intent = Intent(context, SalonDetailsActivity::class.java)
                intent.putExtra("salonModel", salonModel)
                activity.startActivity(intent)
            }
        }

        override fun getItemCount(): Int
        {
            return salonModels.size
        }

        internal inner class salonsVH(itemView: View) : RecyclerView.ViewHolder(itemView)
        {
            var salonTitle: TextView = itemView.findViewById(R.id.salon_title)
            var salonImage: ImageView = itemView.findViewById(R.id.salon_image)
            var salonVisa: ImageView = itemView.findViewById(R.id.salon_visa)
        }
    }

    private fun getSlider()
    {
        databaseReference!!.child("slider_images").addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                slider.clear()

                for (dataSnapshot1 in p0.children)
                {
                    val sliderModel = dataSnapshot1.getValue(sliderModel::class.java)
                    slider.add(sliderModel!!)
                }

                imageSlider.sliderAdapter = SliderAdapterExample(slider)
                images_recycler.adapter = imagesAdapter(activity!!, slider, context!!)

                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    class SliderAdapterExample internal constructor(var img: MutableList<sliderModel>) : SliderViewAdapter<SliderAdapterExample.SliderAdapterVH>()
    {
        override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {
            val inflate = LayoutInflater.from(parent.context).inflate(R.layout.slide_item, parent, false)
            return SliderAdapterVH(inflate)
        }

        override fun onBindViewHolder(viewHolder: SliderAdapterVH, position: Int)
        {
            val url = img[position]
            val image = url.imageUrl

            Picasso.get()
                .load(image)
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(viewHolder.imageViewBackground)
        }

        override fun getCount(): Int { //slider view count could be dynamic size
            return img.size
        }

        inner class SliderAdapterVH(itemView: View) : ViewHolder(itemView) {
            var imageViewBackground: ImageView

            init {
                imageViewBackground = itemView.findViewById(R.id.image_slide)
            }
        }
    }

    internal class imagesAdapter(var activity: FragmentActivity, var sliders: MutableList<sliderModel>, var context: Context) : RecyclerView.Adapter<imagesAdapter.sliderVH>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): sliderVH
        {
            val view = LayoutInflater.from(context).inflate(R.layout.home_image_item, parent, false)
            return sliderVH(view)
        }

        override fun onBindViewHolder(holder: sliderVH, position: Int)
        {
            val slider = sliders[position]
            val image = slider.imageUrl

            Picasso.get()
                .load(image)
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(holder.image)
        }

        override fun getItemCount(): Int
        {
            return sliders.size
        }

        internal inner class sliderVH(itemView: View) : RecyclerView.ViewHolder(itemView)
        {
            var image: ImageView = itemView.findViewById(R.id.home_image)
        }
    }

    internal class mainServicesAdapter(var activity: FragmentActivity, var mainServiceModel: MutableList<mainServiceModel>, var context: Context) : RecyclerView.Adapter<mainServicesAdapter.mainServicesVH>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): mainServicesVH
        {
            val view = LayoutInflater.from(context).inflate(R.layout.home_service_item, parent, false)
            return mainServicesVH(view)
        }

        override fun onBindViewHolder(holder: mainServicesVH, position: Int)
        {
            val mainServiceModel = mainServiceModel[position]
            val english = mainServiceModel.titleEn
            val arabic = mainServiceModel.titleAr
            val image = mainServiceModel.image

            Picasso.get()
                .load(image)
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(holder.serviceImage)

            holder.serviceTitle.text = english
        }

        override fun getItemCount(): Int
        {
            return mainServiceModel.size
        }

        internal inner class mainServicesVH(itemView: View) : RecyclerView.ViewHolder(itemView)
        {
            var serviceImage: CircleImageView = itemView.findViewById(R.id.service_image)
            var serviceTitle: TextView = itemView.findViewById(R.id.service_title)
        }
    }
}