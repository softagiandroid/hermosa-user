package softagi.ss.hermosauser.ui.register

import android.app.ActivityOptions
import android.app.ProgressDialog
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.userModel
import softagi.ss.hermosauser.ui.login.LoginActivity
import softagi.ss.hermosauser.ui.main.MainActivity
import java.util.*

class RegisterActivity : AppCompatActivity()
{
    private var auth:FirebaseAuth? = null
    private var database:FirebaseDatabase? = null
    private var reference:DatabaseReference? = null
    private var progressDialog:ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        //setArabic()
        setContentView(R.layout.activity_register)

        initFirebase()
        fullScreen()
        initToolbar()
        initProgress()
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
    }

    private fun initFirebase()
    {
        auth = FirebaseAuth.getInstance()
        database = FirebaseDatabase.getInstance()
        reference = database!!.reference
    }

    private fun initToolbar()
    {
        val toolbar =
            findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_white_24dp)
        supportActionBar!!.title = resources.getString(R.string.registernow)
    }

    private fun fullScreen()
    {
        val flags = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        window.decorView.systemUiVisibility = flags
        val decorView = window.decorView
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0)
            {
                decorView.systemUiVisibility = flags
            }
        }
    }

    private fun setArabic()
    {
        val locale = Locale("ar")
        Locale.setDefault(locale)
        // Create a new configuration object
        val config = Configuration()
        // Set the locale of the new configuration
        config.locale = locale
        // Update the configuration of the Accplication context
        resources.updateConfiguration(
            config,
            resources.displayMetrics)
    }

    fun register(view: View)
    {
        val firstName = fname_field.text.toString()
        val lastName = lname_field.text.toString()
        val email = email_field.text.toString()
        val password = password_field.text.toString()
        val confirmPassword = confirm_field.text.toString()
        val city = city_field.text.toString()
        val mobile = mobile_field.text.toString()

        if (firstName.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.firstNameError), Toast.LENGTH_SHORT).show()
            return
        }

        if (lastName.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.lastNameError), Toast.LENGTH_SHORT).show()
            return
        }

        if (email.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.emailFieldError), Toast.LENGTH_SHORT).show()
            return
        }

        if (password.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.passwordFieldError), Toast.LENGTH_SHORT).show()
            return
        }

        if (confirmPassword != password)
        {
            Toast.makeText(baseContext, resources.getString(R.string.confirmPasswordFieldError), Toast.LENGTH_SHORT).show()
            return
        }

        if (city.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.cityFieldError), Toast.LENGTH_SHORT).show()
            return
        }

        if (mobile.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.mobileFieldError), Toast.LENGTH_SHORT).show()
            return
        }

        progressDialog!!.show()
        userRegister(firstName,lastName,email,password,city,mobile)
    }

    private fun userRegister(firstName: String, lastName: String, email: String, password: String, city: String, mobile: String)
    {
        auth!!.createUserWithEmailAndPassword(email,password).addOnCompleteListener {
            if (it.isSuccessful)
            {
                it.result!!.user!!.getIdToken(true).addOnCompleteListener { tokenResult ->
                    val token = tokenResult.result!!.token
                    createUser(email,firstName,lastName,city,mobile,token!!,it.result!!.user!!.uid)
                }
            } else
            {
                progressDialog!!.dismiss()
            }
        }
    }

    private fun createUser(
        email: String,
        firstName: String,
        lastName: String,
        city: String,
        mobile: String,
        token: String,
        uid: String
    )
    {
        val user = userModel(firstName, lastName, email, city, mobile,token, uid)
        reference!!.child("users").child(uid).setValue(user)

        progressDialog!!.dismiss()

        startActivity(Intent(applicationContext, MainActivity::class.java))
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        if (item.itemId == android.R.id.home)
        {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}