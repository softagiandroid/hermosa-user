/*
 * Author Abdullah Mansour on 4/4/20 9:42 PM
 * Copyright (c) 2020 . All rights reserved.
 */

package softagi.ss.hermosauser.ui

interface onNavigate
{
    fun onNavigate(to: String)
}