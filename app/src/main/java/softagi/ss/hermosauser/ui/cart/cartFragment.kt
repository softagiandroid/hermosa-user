package softagi.ss.hermosauser.ui.cart

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.cart_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.ui.cart.home.cartHomeFragment
import softagi.ss.hermosauser.ui.cart.salon.cartSalonFragment
import softagi.ss.hermosauser.ui.onNavigate
import softagi.ss.hermosauser.utils.getLanguage
import kotlin.collections.ArrayList

class cartFragment:Fragment(R.layout.cart_fragment)
{
    var onNavigate: onNavigate? = null

    override fun onAttach(context: Context)
    {
        super.onAttach(context)
        onNavigate = context as onNavigate
        onNavigate!!.onNavigate("cart")
    }

    override fun onResume()
    {
        super.onResume()
        onNavigate!!.onNavigate("cart")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        initTabLayout()
    }

    private fun initTabLayout()
    {
        setupViewPager(viewpager)
        tabs.setupWithViewPager(viewpager)
    }

    private fun setupViewPager(v: ViewPager)
    {
        val adapter = ViewPagerAdapter(childFragmentManager)
        Log.d("lang", getLanguage())
        if (getLanguage() == "ar")
        {
            tabs.layoutDirection = View.LAYOUT_DIRECTION_LTR

            adapter.addFragment(cartHomeFragment(), resources.getString(R.string.atHome))
            adapter.addFragment(cartSalonFragment(), resources.getString(R.string.atSalon))
            v.adapter = adapter
            v.currentItem = 1
        } else
        {
            adapter.addFragment(cartSalonFragment(), resources.getString(R.string.atSalon))
            adapter.addFragment(cartHomeFragment(), resources.getString(R.string.atHome))
            v.adapter = adapter
        }
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager)
    {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment
        {
            return mFragmentList[position]
        }

        override fun getCount(): Int
        {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String)
        {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence
        {
            return mFragmentTitleList[position]
        }
    }
}