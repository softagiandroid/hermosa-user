package softagi.ss.hermosauser.ui.login

import android.app.ActivityOptions
import android.app.ProgressDialog
import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.email_field
import kotlinx.android.synthetic.main.activity_register.password_field
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.ui.main.MainActivity
import softagi.ss.hermosauser.ui.register.RegisterActivity
import java.util.*

class LoginActivity : AppCompatActivity()
{
    private var auth: FirebaseAuth? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        //setArabic()
        setContentView(R.layout.activity_login)

        fullScreen()
        initFirebase()
        initProgress()
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
    }

    private fun initFirebase()
    {
        auth = FirebaseAuth.getInstance()
    }

    private fun fullScreen()
    {
        val flags = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)

        window.decorView.systemUiVisibility = flags
        val decorView = window.decorView
        decorView.setOnSystemUiVisibilityChangeListener { visibility ->
            if (visibility and View.SYSTEM_UI_FLAG_FULLSCREEN == 0)
            {
                decorView.systemUiVisibility = flags
            }
        }
    }

    private fun setArabic()
    {
        val locale = Locale("ar")
        Locale.setDefault(locale)
        // Create a new configuration object
        val config = Configuration()
        // Set the locale of the new configuration
        config.locale = locale
        // Update the configuration of the Accplication context
        resources.updateConfiguration(
            config,
            resources.displayMetrics)
    }

    fun login(view: View)
    {
        val email = email_field.text.toString()
        val password = password_field.text.toString()

        if (email.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.emailFieldError), Toast.LENGTH_SHORT).show()
            return
        }

        if (password.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.passwordFieldError), Toast.LENGTH_SHORT).show()
            return
        }

        progressDialog!!.show()
        userLogin(email,password)
    }

    private fun userLogin(email: String, password: String)
    {
        auth!!.signInWithEmailAndPassword(email,password).addOnCompleteListener {
            if (it.isSuccessful)
            {
                progressDialog!!.dismiss()

                startActivity(Intent(applicationContext, MainActivity::class.java))
                finish()
            }
        }
    }

    fun register(view: View)
    {
        startActivity(Intent(applicationContext, RegisterActivity::class.java))
    }
}