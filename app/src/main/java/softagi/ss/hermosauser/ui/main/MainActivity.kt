package softagi.ss.hermosauser.ui.main

import android.Manifest
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.tokenModel
import softagi.ss.hermosauser.utils.initSharedPreferences
import softagi.ss.hermosauser.utils.savePrimitive
import softagi.ss.hermosauser.utils.setArabic
import java.util.*

class MainActivity : AppCompatActivity()
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setArabic(this)
        setContentView(R.layout.activity_main)

        setupNavigation()
        initFirebase()
        locationPermission()
        initSharedPreferences(this)
    }

    override fun onStart() {
        super.onStart()
        savePrimitive("isRunning", false)
    }

    private fun locationPermission()
    {
        ActivityCompat.requestPermissions(
            this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            100
        )
    }

    private fun initFirebase()
    {
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)

        updateToken(databaseReference!!)
    }

    private fun setupNavigation()
    {
        val navController = findNavController(R.id.nav_host_fragment)
        navController.setGraph(R.navigation.nav_graph)
        navigation.setupWithNavController(navController)
    }

    fun updateToken(databaseReference: DatabaseReference)
    {
        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener {
                val uID = FirebaseAuth.getInstance().currentUser!!.uid
                val token = it.token
                val tokenModel = tokenModel(token, uID)
                databaseReference.child("tokens").child(uID).setValue(tokenModel)
            }
    }

    override fun onBackPressed()
    {
        finishAffinity()
    }
}