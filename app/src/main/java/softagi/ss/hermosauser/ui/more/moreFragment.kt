package softagi.ss.hermosauser.ui.more

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.more_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.ui.cart.cartFragment
import softagi.ss.hermosauser.ui.login.LoginActivity
import softagi.ss.hermosauser.ui.secondery.SeconderyActivity
import softagi.ss.hermosauser.utils.getPrimitive
import softagi.ss.hermosauser.utils.initSharedPreferences
import softagi.ss.hermosauser.utils.navigateTo

class moreFragment:Fragment(R.layout.more_fragment)
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var progressDialog: ProgressDialog? = null

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        val uId = FirebaseAuth.getInstance().currentUser!!.uid
        initViews()
        initFirebase()
        initProgress()
        //getData(uId)
        initSharedPreferences(requireActivity())
    }

    private fun initViews()
    {
        cart_linear.setOnClickListener {
            val run = getPrimitive("isRunning", false) as Boolean
            if (run)
            {
                navigateTo(this, cartFragment(), false)
            } else
            {
                val intent = Intent(context, SeconderyActivity::class.java)
                intent.putExtra("from", "cart")
                startActivity(intent)
            }
        }

        logout_linear.setOnClickListener {
            FirebaseAuth.getInstance().signOut()
            val intent = Intent(context, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    /*private fun getData(uId: String)
    {
        databaseReference!!.child("accepted_salons").child(uId).addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                if (p0.hasChildren())
                {
                    status = true
                    val salonModel = p0.getValue(salonModel::class.java)
                    salon_title.text = salonModel!!.salonName

                    if (salonModel.salonImage!!.isNotEmpty())
                    {
                        Picasso.get()
                            .load(salonModel.salonImage)
                            .placeholder(com.google.firebase.database.R.drawable.logo2)
                            .error(com.google.firebase.database.R.drawable.logo2)
                            .into(salon_image)
                    }

                    progressDialog!!.dismiss()
                } else
                {
                    status = false
                    progressDialog!!.dismiss()
                    Toast.makeText(context, resources.getString(com.google.firebase.database.R.string.not_accept_salon_error), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }*/

    private fun initFirebase()
    {
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        //progressDialog!!.show()
    }
}