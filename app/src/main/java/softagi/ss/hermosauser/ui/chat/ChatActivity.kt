package softagi.ss.hermosauser.ui.chat

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_chat.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.chatModel
import softagi.ss.hermosauser.models.notificationModel
import softagi.ss.hermosauser.models.salonModel
import softagi.ss.hermosauser.models.userModel
import java.util.*

class ChatActivity : AppCompatActivity()
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var list:MutableList<chatModel> = mutableListOf()
    private var progressDialog: ProgressDialog? = null
    private var salonModel:salonModel? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setEnglish()
        setContentView(R.layout.activity_chat)

        initToolbar()
        initProgress()
        initFirebase()
        getData()
    }

    private fun initToolbar()
    {
        chat_recycler.layoutManager = LinearLayoutManager(baseContext, RecyclerView.VERTICAL, false)

        salonModel = intent.getSerializableExtra("salonModel") as salonModel

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_white_24dp)
        supportActionBar!!.title = salonModel!!.salonName
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    private fun getData()
    {
        databaseReference!!.child("chats").child(getUid()).child(salonModel!!.uId!!).addValueEventListener(object : ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                list.clear()

                for (dataSnapshot1 in p0.children)
                {
                    val chatModel = dataSnapshot1.getValue(chatModel::class.java)
                    list.add(chatModel!!)
                }

                chat_recycler.adapter  = chatAdapter(getUid(), this@ChatActivity, list, baseContext!!)
                chat_recycler.scrollToPosition(list.size - 1)
                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun initFirebase()
    {
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }

    private fun setEnglish()
    {
        val locale = Locale("en")
        Locale.setDefault(locale)
        // Create a new configuration object
        val config = Configuration()
        // Set the locale of the new configuration
        config.locale = locale
        // Update the configuration of the Accplication context
        resources.updateConfiguration(
            config,
            resources.displayMetrics)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        if (item.itemId == android.R.id.home)
        {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getUid():String
    {
        return FirebaseAuth.getInstance().uid.toString()
    }

    internal class chatAdapter(var myId:String, var activity: FragmentActivity, var chatList: MutableList<chatModel>, var context: Context) : RecyclerView.Adapter<chatAdapter.chatAdapter>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): chatAdapter
        {
            val view = LayoutInflater.from(context).inflate(R.layout.chat_item, parent, false)
            return chatAdapter(view)
        }

        override fun onBindViewHolder(holder: chatAdapter, position: Int)
        {
            val chatModel = chatList[position]
            val message = chatModel.message
            val sender = chatModel.senderId
            val type = chatModel.type

            holder.message.text = message

            if (sender!! == myId)
            {
                holder.linear.gravity = Gravity.END
            } else
            {
                holder.inside.setBackgroundColor(activity.resources.getColor(R.color.white))
                holder.message.setTextColor(activity.resources.getColor(R.color.black))
                holder.time.setTextColor(activity.resources.getColor(R.color.black))
            }

            if (type == 2)
            {
                holder.message.visibility = View.GONE
                holder.image.visibility = View.VISIBLE

                Picasso.get()
                    .load(message)
                    .placeholder(R.drawable.logo2)
                    .error(R.drawable.logo2)
                    .into(holder.image)

                holder.itemView.setOnClickListener {
                    /*val intent = Intent(context, FullImageActivity::class.java)
                    intent.putExtra("image", message)
                    activity.startActivity(intent)*/
                }
            }
        }

        override fun getItemCount(): Int
        {
            return chatList.size
        }

        internal inner class chatAdapter(itemView: View) : RecyclerView.ViewHolder(itemView)
        {
            var message: TextView = itemView.findViewById(R.id.message_txt)
            var image: ImageView = itemView.findViewById(R.id.chat_image)
            var time: TextView = itemView.findViewById(R.id.time_txt)
            var linear: LinearLayout = itemView.findViewById(R.id.chat_linear)
            var inside: LinearLayout = itemView.findViewById(R.id.chat_inside_linear)
        }
    }

    fun send(view: View)
    {
        val message = message_field.text.toString()

        if (message.isEmpty())
        {
            Toast.makeText(baseContext, resources.getString(R.string.send_message), Toast.LENGTH_SHORT).show()
            return
        }

        sendMessage(message)
    }

    private fun sendMessage(message: String)
    {
        val chatModel = chatModel(message, getUid(), 1)
        val key = databaseReference!!.child("chats").child(getUid()).child(salonModel!!.uId!!).push().key
        databaseReference!!.child("chats").child(getUid()).child(salonModel!!.uId!!).child(key!!).setValue(chatModel)
        databaseReference!!.child("chats").child(salonModel!!.uId!!).child(getUid()).child(key).setValue(chatModel)

        val notificationModel = notificationModel("لديك إشعار جديد، رجاء الفحص", "لديك إشعار جديد", 1)

        databaseReference!!.child("notifications").child(salonModel!!.uId!!).child(key).setValue(notificationModel)

        databaseReference!!.child("users").child(getUid()).addListenerForSingleValueEvent(object : ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                val userModel = p0.getValue(userModel::class.java)

                databaseReference!!.child("my_chats").child(getUid()).child(salonModel!!.uId!!).setValue(salonModel)
                databaseReference!!.child("my_chats").child(salonModel!!.uId!!).child(getUid()).setValue(userModel)
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })

        message_field.setText("")
    }

    fun image(view: View)
    {
        val pickPhoto = Intent(
            Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
        startActivityForResult(pickPhoto, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_CANCELED)
        {
            when (requestCode)
            {
                1 -> if (resultCode == Activity.RESULT_OK)
                {
                    progressDialog!!.show()
                    val selectedImage: Uri? = data!!.data
                    uploadImage(selectedImage)
                }
            }
        }

        /*if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            val result = CropImage.getActivityResult(data)
            if (resultCode == Activity.RESULT_OK)
            {
                if (result != null)
                {
                    photoPath = result.uri

                    progressDialog!!.show()
                    uploadImage(photoPath)
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE)
            {
                if (result != null)
                {
                    val error = result.error
                }
            }
        }*/
    }

    private fun uploadImage(photoPath: Uri?)
    {
        val riversRef = FirebaseStorage.getInstance().reference.child("chat_images/${photoPath!!.lastPathSegment}")
        val uploadTask = riversRef.putFile(photoPath)

        val urlTask = uploadTask.continueWithTask { task ->
            if (!task.isSuccessful)
            {
                task.exception?.let {
                    throw it
                }
            }
            riversRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful)
            {
                progressDialog!!.dismiss()
                val downloadUri = task.result

                val chatModel = chatModel(downloadUri.toString(), getUid(), 2)
                val key = databaseReference!!.child("chats").child(getUid()).child(salonModel!!.uId!!).push().key
                databaseReference!!.child("chats").child(getUid()).child(salonModel!!.uId!!).child(key!!).setValue(chatModel)
                databaseReference!!.child("chats").child(salonModel!!.uId!!).child(getUid()).child(key).setValue(chatModel)

                databaseReference!!.child("users").child(getUid()).addListenerForSingleValueEvent(object : ValueEventListener
                {
                    override fun onDataChange(p0: DataSnapshot)
                    {
                        val userModel = p0.getValue(userModel::class.java)

                        databaseReference!!.child("my_chats").child(getUid()).child(salonModel!!.uId!!).setValue(salonModel)
                        databaseReference!!.child("my_chats").child(salonModel!!.uId!!).child(getUid()).setValue(userModel)
                    }

                    override fun onCancelled(p0: DatabaseError)
                    {

                    }
                })
            } else
            {
                // Handle failures
                // ...
            }
        }
    }
}