package softagi.ss.hermosauser.ui.salons

import android.Manifest
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.home_fragment.salons_recycler
import kotlinx.android.synthetic.main.salons_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.salonModel
import softagi.ss.hermosauser.ui.chat.ChatActivity
import softagi.ss.hermosauser.ui.details.SalonDetailsActivity

class salonsFragment:Fragment(R.layout.salons_fragment)
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var list:MutableList<salonModel> = mutableListOf()
    private var progressDialog: ProgressDialog? = null

    var visa: CheckBox? = null
    var deposit: CheckBox? = null
    var filterBtn: Button? = null

    var isVisa: Boolean? = null
    var isDeposit: Boolean? = null

    var myLat: Double? = null
    var myLong:Double? = null
    var raduis:Double? = 20000.0

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        initFirebase()
        initProgress()
        getData(isVisa, isDeposit, myLat, myLong,raduis)
        setFilter()
        clearFilter()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        getNearest()
    }

    private fun getNearest()
    {
        nearest_card.setOnClickListener {
            if (ContextCompat.checkSelfPermission(
                    context!!,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            )
            {
                progressDialog!!.show()
                getMyLocation()
            } else
            {
                ActivityCompat.requestPermissions(
                    activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    100
                )

                progressDialog!!.show()
                getMyLocation()
            }
        }
    }

    private fun getMyLocation()
    {
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location : Location? ->
                // Got last known location. In some rare situations this can be null.
                //Toast.makeText(context, location!!.latitude.toString() + "\n" + location.longitude.toString(), Toast.LENGTH_SHORT).show()

                myLat = location!!.latitude
                myLong = location.longitude

                getData(null, null, myLat, myLong, raduis)
            }
    }

    private fun clearFilter()
    {
        clear_filter.setOnClickListener {
            filter_text.text = resources.getString(R.string.filter)

            isVisa = null
            isDeposit = null
            myLat = null
            myLong = null

            progressDialog!!.show()
            getData(isVisa, isDeposit,myLat, myLong,raduis)
        }
    }

    private fun setFilter() {
        filter_text.setOnClickListener {
            filterDialog()
        }
    }

    private fun filterDialog()
    {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.filter_dialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT

        visa = dialog.findViewById(R.id.visa_check_box) as CheckBox
        deposit = dialog.findViewById(R.id.deposit_check_box) as CheckBox

        filterBtn = dialog.findViewById(R.id.filter_btn) as Button

        filterBtn!!.setOnClickListener {
            isVisa = visa!!.isChecked
            isDeposit = deposit!!.isChecked

            getData(isVisa,isDeposit,myLat,myLong,raduis)
            dialog.dismiss()
            progressDialog!!.show()
        }

        dialog.show()
        dialog.window!!.attributes = lp
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    private fun getData(visa: Boolean?, deposit: Boolean?, latitude: Double?, longitude: Double?, rad:Double?)
    {
        databaseReference!!.child("accepted_salons").addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                list.clear()

                for (dataSnapshot1 in p0.children)
                {
                    val salonModel = dataSnapshot1.getValue(salonModel::class.java)

                    if (latitude != null && longitude != null)
                    {
                        val loc1 = Location("")
                        loc1.latitude = latitude
                        loc1.longitude = longitude

                        val loc2 = Location("")

                        if (salonModel!!.salonLatitude!!.isNotEmpty())
                        {
                            loc2.latitude = salonModel.salonLatitude!!.toDouble()
                            loc2.longitude = salonModel.salonLongitude!!.toDouble()

                            val distanceInMeters = loc1.distanceTo(loc2)

                            if (distanceInMeters <= rad!!)
                            {
                                if (visa == true && deposit == true)
                                {
                                    filter_text.text = resources.getString(R.string.visaDepositFilter)
                                    if (salonModel.visa!! && salonModel.deposit!!)
                                    {
                                        list.add(salonModel)
                                    }
                                } else if (visa == true && deposit == false)
                                {
                                    filter_text.text = resources.getString(R.string.visaFilter)
                                    if (salonModel.visa!!)
                                    {
                                        list.add(salonModel)
                                    }
                                } else if (visa == false && deposit == true)
                                {
                                    filter_text.text = resources.getString(R.string.depositFilter)
                                    if (salonModel.deposit!!)
                                    {
                                        list.add(salonModel)
                                    }
                                } else
                                {
                                    list.add(salonModel)
                                }
                            }
                        }
                    } else
                    {
                        if (salonModel!!.salonLatitude!!.isNotEmpty())
                        {
                            if (visa == true && deposit == true)
                            {
                                filter_text.text = resources.getString(R.string.visaDepositFilter)
                                if (salonModel.visa!! && salonModel.deposit!!)
                                {
                                    list.add(salonModel)
                                }
                            } else if (visa == true && deposit == false)
                            {
                                filter_text.text = resources.getString(R.string.visaFilter)
                                if (salonModel.visa!!)
                                {
                                    list.add(salonModel)
                                }
                            } else if (visa == false && deposit == true)
                            {
                                filter_text.text = resources.getString(R.string.depositFilter)
                                if (salonModel.deposit!!)
                                {
                                    list.add(salonModel)
                                }
                            } else
                            {
                                list.add(salonModel)
                            }
                        }
                    }
                }

                if (list.size == 0)
                {
                    raduis = raduis?.plus(20000.0)
                    getData(isVisa, isDeposit, myLat, myLong, raduis)
                } else
                {
                    salons_recycler.adapter  = salonsAdapter(activity!!, list, context!!)
                }

                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun initFirebase()
    {
        salons_recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val dividerItemDecoration = DividerItemDecoration(context, RecyclerView.VERTICAL)
        salons_recycler.addItemDecoration(dividerItemDecoration)

        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }

    internal class salonsAdapter(var activity: FragmentActivity, var salonModels: MutableList<salonModel>, var context: Context) : RecyclerView.Adapter<salonsAdapter.salonsVH>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): salonsVH
        {
            val view = LayoutInflater.from(context).inflate(R.layout.salon_item, parent, false)
            return salonsVH(view)
        }

        override fun onBindViewHolder(holder: salonsVH, position: Int)
        {
            val salonModel = salonModels[position]
            val title = salonModel.salonName
            val address = salonModel.salonAddress
            val image = salonModel.salonImage
            val visa = salonModel.visa
            val id = salonModel.uId

            holder.salonTitle.text = title

            if (address!!.isEmpty())
            {
                holder.salonAddress.text = activity.resources.getString(R.string.emptyAddress)
            } else
            {
                holder.salonAddress.text = address
            }

            if (visa!!)
            {
                holder.salonVisa.visibility = View.VISIBLE
            }

            if (image!!.isNotEmpty())
            {
                Picasso.get()
                    .load(image)
                    .placeholder(R.drawable.logo2)
                    .error(R.drawable.logo2)
                    .into(holder.salonImage)
            }

            holder.itemView.setOnClickListener {
                val intent = Intent(context, SalonDetailsActivity::class.java)
                intent.putExtra("salonModel", salonModel)
                activity.startActivity(intent)
            }
        }

        override fun getItemCount(): Int
        {
            return salonModels.size
        }

        internal inner class salonsVH(itemView: View) : RecyclerView.ViewHolder(itemView)
        {
            var salonTitle: TextView = itemView.findViewById(R.id.salon_title)
            var salonAddress: TextView = itemView.findViewById(R.id.salon_address)
            var salonImage: ImageView = itemView.findViewById(R.id.salon_image)
            var salonVisa: ImageView = itemView.findViewById(R.id.salon_visa)
        }
    }
}