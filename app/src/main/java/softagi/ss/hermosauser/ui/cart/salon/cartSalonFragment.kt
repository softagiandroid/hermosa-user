package softagi.ss.hermosauser.ui.cart.salon

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.cart_salon_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.cartModel
import softagi.ss.hermosauser.ui.cart.cartFragment
import softagi.ss.hermosauser.ui.main.MainActivity
import softagi.ss.hermosauser.utils.navigateTo

class cartSalonFragment:Fragment(R.layout.cart_salon_fragment)
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var list:MutableList<cartModel> = mutableListOf()
    private var progressDialog: ProgressDialog? = null
    private var total: Int? = 0
    private var loginPrefsEditor: SharedPreferences.Editor? = null
    private lateinit var uId:String

    var timePicker: DatePicker? = null
    var addBtn: Button? = null

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        initPref()
        initFirebase()
        initProgress()
        uId = FirebaseAuth.getInstance().currentUser!!.uid
        getData(uId)
        checkOut()
    }

    private fun checkOut()
    {
        checkout_btn.setOnClickListener {
            dateDialog()
//            progressDialog!!.show()
//
//            val key = databaseReference!!.child("bookings").child(uId).push().key
//
//            val booking = bookingModel(
//                key,
//                uId,
//                list
//            )
//
//            val bookingValue = booking.toMap()
//
//            val childUpdates = HashMap<String, Any>()
//            childUpdates["/bookings/$uId/$key"] = bookingValue
//
//            databaseReference!!.updateChildren(childUpdates).addOnCompleteListener {
//                if (it.isSuccessful)
//                {
//                    Toast.makeText(context, activity!!.resources.getString(R.string.added_to_bookings), Toast.LENGTH_SHORT).show()
//                    progressDialog!!.dismiss()
//                }
//            }
        }
    }

    @SuppressLint("CommitPrefEdits")
    private fun initPref()
    {
        val loginPreferences = activity!!.getSharedPreferences("loginPrefs", Context.MODE_PRIVATE)
        loginPrefsEditor = loginPreferences.edit()
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    private fun getData(uId: String?)
    {
        databaseReference!!.child("cart").child("salons").child(uId!!).addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(p0: DataSnapshot)
            {
                list.clear()

                if (p0.hasChildren())
                {
                    cart_linear.visibility = View.VISIBLE
                    empty_cart_linear.visibility = View.GONE

                    for (dataSnapshot1 in p0.children)
                    {
                        val cartModel = dataSnapshot1.getValue(cartModel::class.java)
                        list.add(cartModel!!)

                        total = total?.plus(cartModel.serviceModel!!.price!!.toInt())
                    }

                    cart_recycler.adapter  = cartAdapter(activity!!, list, context!!, databaseReference!!,uId, this@cartSalonFragment)
                    cart_total.text = resources.getString(R.string.total_cart) + " $total " + list[0].serviceModel!!.currency
                } else
                {
                    cart_linear.visibility = View.GONE
                    empty_cart_linear.visibility = View.VISIBLE
                }
                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun initFirebase()
    {
        cart_recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val dividerItemDecoration = DividerItemDecoration(context, RecyclerView.VERTICAL)
        cart_recycler.addItemDecoration(dividerItemDecoration)

        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)

        discover_now_btn.setOnClickListener {
            activity!!.finish()
            val intent = Intent(context, MainActivity::class.java)
            startActivity(intent)
        }
    }

    internal class cartAdapter(
        var activity: FragmentActivity,
        var cartModel: MutableList<cartModel>,
        var context: Context,
        var databaseReference: DatabaseReference,
        var uId: String,
        var cartFragment: cartSalonFragment
    ) : RecyclerView.Adapter<cartAdapter.cartVH>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): cartVH
        {
            val view = LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false)
            return cartVH(view)
        }

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: cartVH, position: Int)
        {
            val cartModel = cartModel[position]
            val id = cartModel.id

            holder.title.text = cartModel.serviceModel!!.titleAr
            holder.bySalon.text = "${activity.resources.getString(R.string.by)} ${cartModel.salonModel!!.salonName}"
            holder.byTeam.text = "${activity.resources.getString(R.string.cart_from)} ${cartModel.teamModel!!.Namear}"
            holder.price.text = "${activity.resources.getString(R.string.price)} ${cartModel.serviceModel!!.price} ${cartModel.serviceModel!!.currency}"
            holder.duration.text = "${activity.resources.getString(R.string.duration)} ${cartModel.serviceModel!!.duration} ${activity.resources.getString(R.string.minutes)}"

            Picasso.get()
                .load(cartModel.serviceModel!!.imageUrl)
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(holder.image)

            holder.more.setOnClickListener {
                val popup = PopupMenu(context, holder.more)
                popup.menuInflater.inflate(R.menu.cart_item_menu, popup.menu)
                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId)
                    {
                        R.id.delete ->
                        {
                            databaseReference.child("cart").child("salons").child(uId).child(cartModel.id!!).removeValue()
                            navigateTo(cartFragment, cartFragment(), false)
                            true
                        }
                        else -> true
                    }
                }
                popup.show() //showing popup menu
            }
        }

        override fun getItemCount(): Int
        {
            return cartModel.size
        }

        internal inner class cartVH(itemView: View) : RecyclerView.ViewHolder(itemView)
        {
            var title: TextView = itemView.findViewById(R.id.service_title)
            var bySalon: TextView = itemView.findViewById(R.id.by_salon_title)
            var byTeam: TextView = itemView.findViewById(R.id.service_team)
            var price: TextView = itemView.findViewById(R.id.service_price)
            var duration: TextView = itemView.findViewById(R.id.service_duration)
            var image: ImageView = itemView.findViewById(R.id.service_image)
            var more: ImageView = itemView.findViewById(R.id.more_image)
        }
    }

    private fun dateDialog()
    {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.date_dialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT

        timePicker = dialog.findViewById(R.id.service_time_picker) as DatePicker
        addBtn = dialog.findViewById(R.id.add_btn) as Button

        addBtn!!.setOnClickListener {
//            progressDialog!!.show()
//            val uId = FirebaseAuth.getInstance().currentUser!!.uid
//            databaseReference!!.child("accepted_salons").child(uId).addListenerForSingleValueEvent(object : ValueEventListener
//            {
//                override fun onDataChange(p0: DataSnapshot)
//                {
//                    if (p0.hasChildren())
//                    {
//                        val salonModel = p0.getValue(salonModel::class.java)
//                        if (salonModel!!.currency!!.isNotEmpty())
//                        {
//                            dialog.dismiss()
//                            uploadImage(selectedImage!!,titleE,titleA,descE,descA,price,timePicker!!.value.toString(),mainServiceName,mainServiceId,salonModel.salonName,salonModel.uId, salonModel.currency, isHome)
//                        } else
//                        {
//                            progressDialog!!.dismiss()
//                            Toast.makeText(context, resources.getString(R.string.select_currency), Toast.LENGTH_SHORT).show()
//                        }
//                    } else
//                    {
//                        progressDialog!!.dismiss()
//                        Toast.makeText(context, resources.getString(R.string.not_accept_salon_error), Toast.LENGTH_SHORT).show()
//                    }
//                }
//
//                override fun onCancelled(p0: DatabaseError)
//                {
//
//                }
//            })
        }

        dialog.show()
        dialog.window!!.attributes = lp
    }
}