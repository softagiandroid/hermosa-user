package softagi.ss.hermosauser.ui.messagingService

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.ui.main.MainActivity

/*class chatMessagingService: FirebaseMessagingService()
{
    private val CHANNEL_ID = "fcm_fallback_notification_channel"
    private var builder: NotificationCompat.Builder? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage)
    {
        cerateNotification(remoteMessage.notification!!.body)
    }

    private fun cerateNotification(body: String?)
    {
        val intent = Intent(applicationContext, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivities(applicationContext, 0, arrayOf(intent), 0)

        builder = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setSmallIcon(R.drawable.logo3)
            .setContentTitle("لديك إشعار جديد")
            .setContentText(body)
            .setStyle(NotificationCompat.BigTextStyle().bigText(body))
            .setVibrate(longArrayOf(Notification.DEFAULT_VIBRATE.toLong()))
            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            .setFullScreenIntent(pendingIntent, true)
            .setPriority(NotificationCompat.PRIORITY_MAX)
        //createNotificationChannel()
        val notificationManager = NotificationManagerCompat.from(applicationContext)
        notificationManager.notify(102, builder!!.build())
    }

    @SuppressLint("WrongConstant")
    private fun createNotificationChannel()
    {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            val name: CharSequence = ""
            val description = ""
            val importance = NotificationManager.IMPORTANCE_MAX
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager =
                applicationContext.getSystemService(
                    NotificationManager::class.java
                )
            notificationManager?.createNotificationChannel(channel)
        }
    }
}*/