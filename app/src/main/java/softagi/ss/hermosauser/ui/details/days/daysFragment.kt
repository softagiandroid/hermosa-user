package softagi.ss.hermosauser.ui.details.days

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.days_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.daysModel
import softagi.ss.hermosauser.models.salonModel

class daysFragment(var salonModel: salonModel?) :Fragment(R.layout.days_fragment)
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var list:MutableList<daysModel> = mutableListOf()
    private var progressDialog: ProgressDialog? = null

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        initFirebase()
        initProgress()
        getData(salonModel!!.uId)
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    private fun getData(uId: String?)
    {
        databaseReference!!.child("salons_appointments").child(uId!!).addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                list.clear()

                if (p0.hasChildren())
                {
                    for (dataSnapshot1 in p0.children)
                    {
                        val daysModel = dataSnapshot1.getValue(daysModel::class.java)
                        list.add(daysModel!!)
                    }

                    setDays(list)
                } else
                {
                    days_linear.visibility = View.GONE
                }
                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun setDays(list: MutableList<daysModel>)
    {
        friday_select.setOnClickListener {
            if (list[0].status == false)
            {
                Toast.makeText(context, resources.getString(R.string.closeDay), Toast.LENGTH_SHORT).show()

                from_to_linear.visibility = View.GONE
                day_select.visibility = View.GONE
            } else
            {
                from_to_linear.visibility = View.VISIBLE
                day_select.visibility = View.VISIBLE

                day_select.text = resources.getString(R.string.friday)
                from.text = list[0].from
                to.text = list[0].to
            }
        }
        if (list[0].status == false)
        {
            friday_image.setImageResource(R.drawable.ic_wrong)
            friday_image.setBackgroundColor(resources.getColor(R.color.red))
        }

        saturday_select.setOnClickListener {
            if (list[1].status == false)
            {
                Toast.makeText(context, resources.getString(R.string.closeDay), Toast.LENGTH_SHORT).show()

                from_to_linear.visibility = View.GONE
                day_select.visibility = View.GONE
            } else
            {
                from_to_linear.visibility = View.VISIBLE
                day_select.visibility = View.VISIBLE

                day_select.text = resources.getString(R.string.saturday)
                from.text = list[1].from
                to.text = list[1].to
            }
        }
        if (list[1].status == false)
        {
            saturday_image.setImageResource(R.drawable.ic_wrong)
            saturday_image.setBackgroundColor(resources.getColor(R.color.red))
        }

        sunday_select.setOnClickListener {
            if (list[2].status == false)
            {
                Toast.makeText(context, resources.getString(R.string.closeDay), Toast.LENGTH_SHORT).show()

                from_to_linear.visibility = View.GONE
                day_select.visibility = View.GONE
            } else
            {
                from_to_linear.visibility = View.VISIBLE
                day_select.visibility = View.VISIBLE

                day_select.text = resources.getString(R.string.sunday)
                from.text = list[2].from
                to.text = list[2].to
            }
        }
        if (list[2].status == false)
        {
            sunday_image.setImageResource(R.drawable.ic_wrong)
            sunday_image.setBackgroundColor(resources.getColor(R.color.red))
        }

        monday_select.setOnClickListener {
            if (list[3].status == false)
            {
                Toast.makeText(context, resources.getString(R.string.closeDay), Toast.LENGTH_SHORT).show()

                from_to_linear.visibility = View.GONE
                day_select.visibility = View.GONE
            } else
            {
                from_to_linear.visibility = View.VISIBLE
                day_select.visibility = View.VISIBLE

                day_select.text = resources.getString(R.string.monday)
                from.text = list[3].from
                to.text = list[3].to
            }
        }
        if (list[3].status == false)
        {
            monday_image.setImageResource(R.drawable.ic_wrong)
            monday_image.setBackgroundColor(resources.getColor(R.color.red))
        }

        tuesday_Select.setOnClickListener {
            if (list[4].status == false)
            {
                Toast.makeText(context, resources.getString(R.string.closeDay), Toast.LENGTH_SHORT).show()

                from_to_linear.visibility = View.GONE
                day_select.visibility = View.GONE
            } else
            {
                from_to_linear.visibility = View.VISIBLE
                day_select.visibility = View.VISIBLE

                day_select.text = resources.getString(R.string.tuesday)
                from.text = list[4].from
                to.text = list[4].to
            }
        }
        if (list[4].status == false)
        {
            tuesday_image.setImageResource(R.drawable.ic_wrong)
            tuesday_image.setBackgroundColor(resources.getColor(R.color.red))
        }

        wednesday_select.setOnClickListener {
            if (list[5].status == false)
            {
                Toast.makeText(context, resources.getString(R.string.closeDay), Toast.LENGTH_SHORT).show()

                from_to_linear.visibility = View.GONE
                day_select.visibility = View.GONE
            } else
            {
                from_to_linear.visibility = View.VISIBLE
                day_select.visibility = View.VISIBLE

                day_select.text = resources.getString(R.string.wednesday)
                from.text = list[5].from
                to.text = list[5].to
            }
        }
        if (list[5].status == false)
        {
            wednesday_image.setImageResource(R.drawable.ic_wrong)
            wednesday_image.setBackgroundColor(resources.getColor(R.color.red))
        }

        thursday_select.setOnClickListener {
            if (list[6].status == false)
            {
                Toast.makeText(context, resources.getString(R.string.closeDay), Toast.LENGTH_SHORT).show()

                from_to_linear.visibility = View.GONE
                day_select.visibility = View.GONE
            } else
            {
                from_to_linear.visibility = View.VISIBLE
                day_select.visibility = View.VISIBLE

                day_select.text = resources.getString(R.string.thursday)
                from.text = list[6].from
                to.text = list[6].to
            }
        }
        if (list[6].status == false)
        {
            thursday_image.setImageResource(R.drawable.ic_wrong)
            thursday_image.setBackgroundColor(resources.getColor(R.color.red))
        }
    }

    private fun initFirebase()
    {
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }
}