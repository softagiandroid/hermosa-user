package softagi.ss.hermosauser.ui.service

import android.annotation.SuppressLint
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.services_fragment.*
import kotlinx.android.synthetic.main.services_fragment.clear_filter
import kotlinx.android.synthetic.main.services_fragment.filter_text
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.mainServiceModel
import softagi.ss.hermosauser.models.salonModel
import softagi.ss.hermosauser.models.serviceModel
import softagi.ss.hermosauser.ui.details.teams.salonServiceTeamFragment
import softagi.ss.hermosauser.ui.secondery.SeconderyActivity
import softagi.ss.hermosauser.utils.getPrimitive
import softagi.ss.hermosauser.utils.initSharedPreferences
import softagi.ss.hermosauser.utils.isHome
import softagi.ss.hermosauser.utils.navigateTo

class serviceFragment:Fragment(R.layout.services_fragment)
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var list:MutableList<serviceModel> = mutableListOf()
    private var progressDialog: ProgressDialog? = null

    val spinnerArray: MutableList<String?> = ArrayList()
    val spinnerIds: MutableList<String?> = ArrayList()
    var mainServiceName: String? = null
    var maxAmount: String? = null
    var minAmount: String? = null
    var isHome:Boolean? = null

    var max: EditText? = null
    var min: EditText? = null
    var serviceSpinner: Spinner? = null
    var home: CheckBox? = null
    var filterBtn: Button? = null

    override fun onActivityCreated(savedInstanceState: Bundle?)
    {
        super.onActivityCreated(savedInstanceState)

        initFirebase()
        initProgress()
        getData(mainServiceName, isHome, maxAmount, minAmount)
        setFilter()
        clearFilter()
        setSearch()
        initSharedPreferences(requireActivity())
    }

    private fun setSearch() {
        search_card.setOnClickListener {
            filter_linear.visibility = View.GONE
            services_search.visibility = View.VISIBLE
        }
    }

    private fun clearFilter()
    {
        clear_filter.setOnClickListener {
            filter_text.text = resources.getString(R.string.filter)

            mainServiceName = null
            isHome = null
            maxAmount = null
            minAmount = null

            progressDialog!!.show()
            getData(mainServiceName, isHome, maxAmount, minAmount)
        }
    }

    private fun setFilter()
    {
        filter_text.setOnClickListener {
            filterDialog()
        }
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(context)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    private fun getData(
        mainServiceName: String?,
        home: Boolean?,
        maxAmount: String?,
        minAmount: String?
    )
    {
        databaseReference!!.child("accept_services").addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                list.clear()

                for (dataSnapshot1 in p0.children)
                {
                    val serviceModel = dataSnapshot1.getValue(serviceModel::class.java)

                    // serviceModel!!.mainServiceTitle.equals(mainServiceName)
                    // serviceModel!!.price!!.toInt() <= maxAmount.toInt()
                    // serviceModel!!.price!!.toInt() >= minAmount.toInt()
                    // serviceModel.home!!

                    if (mainServiceName == null && home != true && maxAmount == null && minAmount != null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.price!!.toInt() >= minAmount.toInt())
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName == null && home != true && maxAmount != null && minAmount == null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.price!!.toInt() <= maxAmount.toInt())
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName == null && home != true && maxAmount != null && minAmount != null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.price!!.toInt() <= maxAmount.toInt() && serviceModel.price!!.toInt() >= minAmount.toInt())
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName == null && home == true && maxAmount == null && minAmount == null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.home!!)
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName == null && home == true && maxAmount == null && minAmount != null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.price!!.toInt() >= minAmount.toInt() && serviceModel.home!!)
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName == null && home == true && maxAmount != null && minAmount == null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.price!!.toInt() <= maxAmount.toInt() && serviceModel.home!!)
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName == null && home == true && maxAmount != null && minAmount != null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.price!!.toInt() <= maxAmount.toInt() && serviceModel.price!!.toInt() >= minAmount.toInt() && serviceModel.home!!)
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName != null && home != true && maxAmount == null && minAmount == null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.mainServiceTitle.equals(mainServiceName))
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName != null && home != true && maxAmount == null && minAmount != null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.mainServiceTitle.equals(mainServiceName) && serviceModel.price!!.toInt() >= minAmount.toInt()
                        )
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName != null && home != true && maxAmount != null && minAmount == null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.mainServiceTitle.equals(mainServiceName) && serviceModel.price!!.toInt() <= maxAmount.toInt())
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName != null && home != true && maxAmount != null && minAmount != null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.mainServiceTitle.equals(mainServiceName) && serviceModel.price!!.toInt() <= maxAmount.toInt() && serviceModel.price!!.toInt() >= minAmount.toInt())
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName != null && home == true && maxAmount == null && minAmount == null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.mainServiceTitle.equals(mainServiceName) && serviceModel.home!!
                        )
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName != null && home == true && maxAmount == null && minAmount != null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.mainServiceTitle.equals(mainServiceName) && serviceModel.price!!.toInt() >= minAmount.toInt() && serviceModel.home!!)
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName != null && home == true && maxAmount != null && minAmount == null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.mainServiceTitle.equals(mainServiceName) && serviceModel.price!!.toInt() <= maxAmount.toInt() && serviceModel.home!!)
                        {
                            list.add(serviceModel)
                        }
                    } else if (mainServiceName != null && home == true && maxAmount != null && minAmount != null)
                    {
                        //filter_text.text = resources.getString(R.string.visaFilter)
                        if (serviceModel!!.mainServiceTitle.equals(mainServiceName) && serviceModel.price!!.toInt() <= maxAmount.toInt() && serviceModel.price!!.toInt() >= minAmount.toInt() && serviceModel.home!!)
                        {
                            list.add(serviceModel)
                        }
                    } else
                    {
                        list.add(serviceModel!!)
                    }
                }

                services_recycler.adapter  = servciesAdapter(activity!!, list, context!!, databaseReference!!, this@serviceFragment)

                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun initFirebase()
    {
        services_recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        val dividerItemDecoration = DividerItemDecoration(context, RecyclerView.VERTICAL)
        services_recycler.addItemDecoration(dividerItemDecoration)

        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }

    internal class servciesAdapter(
        var activity: FragmentActivity,
        var serviceModel: MutableList<serviceModel>,
        var context: Context,
        var databaseReference: DatabaseReference,
        var serviceFragment: serviceFragment
    ) : RecyclerView.Adapter<servciesAdapter.servicesVH>()
    {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): servicesVH
        {
            val view = LayoutInflater.from(context).inflate(R.layout.service_item, parent, false)
            return servicesVH(view)
        }

        @SuppressLint("SetTextI18n")
        override fun onBindViewHolder(holder: servicesVH, position: Int)
        {
            val serviceModel = serviceModel[position]
            val image = serviceModel.imageUrl
            val titleE = serviceModel.titleEn
            val salonName = serviceModel.salonName
            val titleA = serviceModel.titleAr
            val descE = serviceModel.descEn
            val descA = serviceModel.descAr
            val price = serviceModel.price
            val currency = serviceModel.currency
            val duration = serviceModel.duration
            val salonId = serviceModel.salonId
            val home = serviceModel.home

            holder.title.text = titleA
            holder.bySalon.text = "${activity.resources.getString(R.string.by)} $salonName"
            holder.price.text = "${activity.resources.getString(R.string.price)} $price $currency"
            holder.duration.text = "${activity.resources.getString(R.string.duration)} $duration ${activity.resources.getString(R.string.minutes)}"

            if (home!!)
            {
                holder.home.visibility = View.VISIBLE
            }

            Picasso.get()
                .load(image)
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(holder.image)

            holder.itemView.setOnClickListener {
                databaseReference.child("accepted_salons").child(salonId!!).addListenerForSingleValueEvent(object : ValueEventListener
                {
                    override fun onCancelled(p0: DatabaseError)
                    {

                    }

                    override fun onDataChange(p0: DataSnapshot)
                    {
                        val salonModel = p0.getValue(salonModel::class.java)

                        val run = getPrimitive("isRunning", false) as Boolean
                        if (run)
                        {
                            navigateTo(serviceFragment, salonServiceTeamFragment(salonModel!!,serviceModel), false)
                        } else
                        {
                            val intent = Intent(context, SeconderyActivity::class.java)
                            intent.putExtra("from", "salon_service")
                            intent.putExtra("salon_model", salonModel)
                            intent.putExtra("service_model", serviceModel)
                            activity.startActivity(intent)

                            if (home)
                            {
                                isHome = true
                            }
                        }
                    }
                })
            }
        }

        override fun getItemCount(): Int
        {
            return serviceModel.size
        }

        internal inner class servicesVH(itemView: View) : RecyclerView.ViewHolder(itemView)
        {
            var title: TextView = itemView.findViewById(R.id.service_title)
            var bySalon: TextView = itemView.findViewById(R.id.by_salon_title)
            var price: TextView = itemView.findViewById(R.id.service_price)
            var duration: TextView = itemView.findViewById(R.id.service_duration)
            var image: ImageView = itemView.findViewById(R.id.service_image)
            var home: ImageView = itemView.findViewById(R.id.service_home)
        }
    }

    private fun filterDialog()
    {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE) // before
        dialog.setContentView(R.layout.service_dialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(true)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window!!.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT


        serviceSpinner = dialog.findViewById(R.id.services_list_spinner) as Spinner
        home = dialog.findViewById(R.id.home_check_box) as CheckBox
        max = dialog.findViewById(R.id.max) as EditText
        min = dialog.findViewById(R.id.min) as EditText
        filterBtn = dialog.findViewById(R.id.filter_btn) as Button

        setSpinner(serviceSpinner!!)

        serviceSpinner!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(parent: AdapterView<*>?)
            {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long)
            {
                mainServiceName = spinnerArray[position]
            }
        }

        filterBtn!!.setOnClickListener {
            isHome = home!!.isChecked

            maxAmount = if (max!!.text.toString().isNotEmpty())
            {
                max!!.text.toString()
            } else
            {
                null
            }

            minAmount = if (min!!.text.toString().isNotEmpty())
            {
                min!!.text.toString()
            } else
            {
                null
            }

            if (mainServiceName!! == resources.getString(R.string.select_main_service))
            {
                mainServiceName = null
            }

            getData(mainServiceName, isHome, maxAmount, minAmount)
            dialog.dismiss()
            progressDialog!!.show()
        }

        dialog.show()
        dialog.window!!.attributes = lp
    }

    private fun setSpinner(spinner: Spinner)
    {
        databaseReference!!.child("main_services").addValueEventListener(object : ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                spinnerArray.clear()
                spinnerArray.add(resources.getString(R.string.select_main_service))
                spinnerIds.add(resources.getString(R.string.select_main_service))

                for (dataSnapshot1 in p0.children)
                {
                    val mainServiceModel = dataSnapshot1.getValue(mainServiceModel::class.java)
                    spinnerArray.add(mainServiceModel!!.titleAr)
                    spinnerIds.add(mainServiceModel.id)
                }

                val spinnerArrayAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_item, spinnerArray)
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                spinner.adapter = spinnerArrayAdapter

                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }
}