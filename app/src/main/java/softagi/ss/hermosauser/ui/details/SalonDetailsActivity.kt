package softagi.ss.hermosauser.ui.details

import android.app.ProgressDialog
import android.content.Intent
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.firebase.database.*
import com.smarteist.autoimageslider.SliderViewAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_salon_details.*
import kotlinx.android.synthetic.main.activity_salon_details.imageSlider
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.chatModel
import softagi.ss.hermosauser.models.salonModel
import softagi.ss.hermosauser.models.sliderModel
import softagi.ss.hermosauser.ui.chat.ChatActivity
import softagi.ss.hermosauser.ui.details.days.daysFragment
import softagi.ss.hermosauser.ui.details.reviews.reviewsFragment
import softagi.ss.hermosauser.ui.details.services.servicesDetailsFragment
import java.util.*
import kotlin.collections.ArrayList

class SalonDetailsActivity : AppCompatActivity()
{
    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var list:MutableList<chatModel> = mutableListOf()
    private var progressDialog: ProgressDialog? = null
    private var salonModel:salonModel? = null
    private var slider:MutableList<sliderModel> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setArabic()
        setContentView(R.layout.activity_salon_details)

        initProgress()
        initToolbar()
        initViews()
        initFirebase()
        setData()
        initTabLayout()
    }

    private fun initViews()
    {
        chat_linear.setOnClickListener {
            val intent = Intent(baseContext, ChatActivity::class.java)
            intent.putExtra("salonModel", salonModel)
            startActivity(intent)
        }
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    private fun initFirebase()
    {
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }

    private fun setData()
    {
        salonModel = intent.getSerializableExtra("salonModel") as salonModel

        if (!salonModel!!.salonImage.isNullOrEmpty())
        {
            Picasso.get()
                .load(salonModel!!.salonImage)
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(salon_image)
        }

        salon_title.text = salonModel!!.salonName
        salon_address.text = salonModel!!.salonAddress

        getSlider(salonModel!!.uId)
    }

    private fun getSlider(uId: String?)
    {
        databaseReference!!.child("gallery_images").child(uId!!).addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            override fun onDataChange(p0: DataSnapshot)
            {
                slider.clear()

                for (dataSnapshot1 in p0.children)
                {
                    val sliderModel = dataSnapshot1.getValue(sliderModel::class.java)
                    slider.add(sliderModel!!)
                }

                imageSlider.sliderAdapter = SliderAdapterExample(slider)

                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    class SliderAdapterExample internal constructor(var img: MutableList<sliderModel>) : SliderViewAdapter<SliderAdapterExample.SliderAdapterVH>()
    {
        override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {
            val inflate = LayoutInflater.from(parent.context).inflate(R.layout.slide_item, parent, false)
            return SliderAdapterVH(inflate)
        }

        override fun onBindViewHolder(viewHolder: SliderAdapterVH, position: Int)
        {
            val url = img[position]
            val image = url.imageUrl

            Picasso.get()
                .load(image)
                .placeholder(R.drawable.logo2)
                .error(R.drawable.logo2)
                .into(viewHolder.imageViewBackground)
        }

        override fun getCount(): Int { //slider view count could be dynamic size
            return img.size
        }

        inner class SliderAdapterVH(itemView: View) : ViewHolder(itemView) {
            var imageViewBackground: ImageView

            init {
                imageViewBackground = itemView.findViewById(R.id.image_slide)
            }
        }
    }

    private fun initToolbar()
    {
        setSupportActionBar(details_toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_white_24dp)
        supportActionBar!!.title = ""
    }

    private fun setArabic()
    {
        val locale = Locale("ar")
        Locale.setDefault(locale)
        // Create a new configuration object
        val config = Configuration()
        // Set the locale of the new configuration
        config.locale = locale
        // Update the configuration of the Accplication context
        resources.updateConfiguration(
            config,
            resources.displayMetrics)
    }

    private fun initTabLayout()
    {
        val current = resources.configuration.locale

        setupViewPager(viewpager,current)
        tabs.setupWithViewPager(viewpager)
        //tabs.getTabAt(0)!!.setIcon(R.drawable.ic_art)

        /*salon_maps.setOnClickListener
        {
            if (data!!.location_lat != null && data!!.location_long != null)
            {
                val gmmIntentUri = Uri.parse("geo:${data!!.location_lat},${data!!.location_long}")
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                if (mapIntent.resolveActivity(packageManager) != null)
                {
                    startActivity(mapIntent)
                }

                /*val url = "https://maps.googleapis.com/maps/api/staticmap?center=\" + \"${data!!.location_lat},${data!!.location_long}\" +\n" +
                        "                    \"&markers=color:blue%7Clabel:S%7C${data!!.location_lat},${data!!.location_long}\" +\n" +
                        "                    \"&zoom=13&size=350x250&maptype=roadmap\\n\" +\n" +
                        "                    \"&key=AIzaSyAPwS2app3RMke4vvHaLcg9tz11Ddr8qI4"

                val uris = Uri.parse(url)
                val intents = Intent(Intent.ACTION_VIEW, uris)
                val b = Bundle()
                b.putBoolean("new_window", true)
                intents.putExtras(b)
                startActivity(intents)*/
            } else
            {
                Toast.makeText(this, "المكان غير محدد" , Toast.LENGTH_SHORT).show()
            }
        }*/
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        if (item.itemId == android.R.id.home)
        {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupViewPager(v: ViewPager, c: Locale)
    {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        if (c.displayName == "العربية")
        {
            tabs.layoutDirection = View.LAYOUT_DIRECTION_LTR

            //adapter.addFragment(ServicesSalonFragment(data!!.id.toString()), resources.getString(R.string.servicessalon))
            adapter.addFragment(reviewsFragment(salonModel), resources.getString(R.string.reviewssalon))
            adapter.addFragment(daysFragment(salonModel), resources.getString(R.string.dayssalon))
            adapter.addFragment(servicesDetailsFragment(salonModel), resources.getString(R.string.servicessalon))

            v.adapter = adapter
            v.currentItem = 2
        } else
        {
            adapter.addFragment(servicesDetailsFragment(salonModel), resources.getString(R.string.servicessalon))
            adapter.addFragment(daysFragment(salonModel), resources.getString(R.string.dayssalon))
            adapter.addFragment(reviewsFragment(salonModel), resources.getString(R.string.reviewssalon))

            v.adapter = adapter
        }
    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager)
    {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment
        {
            return mFragmentList[position]
        }

        override fun getCount(): Int
        {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String)
        {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence
        {
            return mFragmentTitleList[position]
        }
    }
}