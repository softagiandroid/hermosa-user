package softagi.ss.hermosauser.ui.secondery

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_secondery.*
import kotlinx.android.synthetic.main.cart_fragment.*
import softagi.ss.hermosauser.R
import softagi.ss.hermosauser.models.cartModel
import softagi.ss.hermosauser.models.salonModel
import softagi.ss.hermosauser.models.serviceModel
import softagi.ss.hermosauser.ui.cart.cartFragment
import softagi.ss.hermosauser.ui.details.teams.salonServiceTeamFragment
import softagi.ss.hermosauser.ui.main.MainActivity
import softagi.ss.hermosauser.ui.onNavigate
import softagi.ss.hermosauser.utils.initSharedPreferences
import softagi.ss.hermosauser.utils.savePrimitive
import softagi.ss.hermosauser.utils.setArabic
import java.nio.file.Files.delete

class SeconderyActivity : AppCompatActivity(), onNavigate
{
    private var cart:Boolean = false
    private var loginPrefsEditor: SharedPreferences.Editor? = null
    private var loginPreferences: SharedPreferences?= null

    private var firebaseDatabase: FirebaseDatabase? = null
    private var databaseReference: DatabaseReference? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setArabic(this)
        setContentView(R.layout.activity_secondery)

        initToolbar()
        initPref()
        loadFragment()
        initFirebase()
        initProgress()
        initSharedPreferences(this)
    }

    override fun onStart()
    {
        super.onStart()
        savePrimitive("isRunning", true)
    }

    override fun onDestroy()
    {
        super.onDestroy()
        savePrimitive("isRunning", false)
    }

    private fun initProgress()
    {
        progressDialog = ProgressDialog(this)
        progressDialog!!.setMessage(resources.getString(R.string.wait))
        progressDialog!!.setCancelable(false)
    }

    private fun getData(uId: String?)
    {
        databaseReference!!.child("cart").child("salons").child(uId!!).addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(p0: DataSnapshot)
            {
                if (p0.hasChildren())
                {
                    for (dataSnapshot1 in p0.children)
                    {
                        val cartModel = dataSnapshot1.getValue(cartModel::class.java)
                        databaseReference!!.child("cart").child("salons").child(uId).child(cartModel!!.id!!).removeValue()
                    }

                    loginPrefsEditor!!.putString("salon_id", "0")
                    loginPrefsEditor!!.apply()
                    Toast.makeText(baseContext, resources.getString(R.string.delete_cart_done), Toast.LENGTH_SHORT).show()

                    val intent = Intent(baseContext, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else
                {
                    Toast.makeText(baseContext, resources.getString(R.string.empty_cart), Toast.LENGTH_SHORT).show()
                }
                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })

        databaseReference!!.child("cart").child("home").child(uId).addListenerForSingleValueEvent(object :
            ValueEventListener
        {
            @SuppressLint("SetTextI18n")
            override fun onDataChange(p0: DataSnapshot)
            {
                if (p0.hasChildren())
                {
                    for (dataSnapshot1 in p0.children)
                    {
                        val cartModel = dataSnapshot1.getValue(cartModel::class.java)
                        databaseReference!!.child("cart").child("home").child(uId).child(cartModel!!.id!!).removeValue()
                    }

                    loginPrefsEditor!!.putString("salon_id", "0")
                    loginPrefsEditor!!.apply()
                    Toast.makeText(baseContext, resources.getString(R.string.delete_cart_done), Toast.LENGTH_SHORT).show()

                    val intent = Intent(baseContext, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else
                {
                    Toast.makeText(baseContext, resources.getString(R.string.empty_cart), Toast.LENGTH_SHORT).show()
                }
                progressDialog!!.dismiss()
            }

            override fun onCancelled(p0: DatabaseError)
            {

            }
        })
    }

    private fun initFirebase()
    {
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase!!.reference
        databaseReference!!.keepSynced(true)
    }

    @SuppressLint("CommitPrefEdits")
    private fun initPref()
    {
        loginPreferences = getSharedPreferences("loginPrefs", Context.MODE_PRIVATE)
        loginPrefsEditor = loginPreferences!!.edit()
    }

    private fun loadFragment()
    {
        when (intent.getStringExtra("from"))
        {
            "salon_service" ->
            {
                val salonModel = intent.getSerializableExtra("salon_model") as salonModel
                val serviceModel = intent.getSerializableExtra("service_model") as serviceModel

                val fragment = salonServiceTeamFragment(salonModel,serviceModel)
                startFragment(fragment)
                supportActionBar!!.title = resources.getString(R.string.teams)
            }

            "cart" ->
            {
                cart = true
                val fragment = cartFragment()
                startFragment(fragment)
                supportActionBar!!.title = resources.getString(R.string.my_cart)
            }
        }
    }

    private fun startFragment(fragment: Fragment)
    {
        supportFragmentManager.beginTransaction().add(R.id.container, fragment).commit()
    }

    private fun initToolbar()
    {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_keyboard_arrow_left_white_24dp)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean
    {
        return when (item.itemId)
        {
            android.R.id.home ->
            {
                onBackPressed()
                true
            }
            R.id.delete_cart ->
            {
                val uId = FirebaseAuth.getInstance().currentUser!!.uid
                getData(uId)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        if (cart)
        {
            val inflater: MenuInflater = menuInflater
            inflater.inflate(R.menu.cart_menu, menu)
            return true
        }

        return true
    }

    override fun onNavigate(to: String)
    {
        when (to)
        {
            "salon_service" ->
            {
                supportActionBar!!.title = resources.getString(R.string.teams)
            }

            "cart" ->
            {
                cart = true
                supportActionBar!!.title = resources.getString(R.string.my_cart)
            }
        }
    }
}