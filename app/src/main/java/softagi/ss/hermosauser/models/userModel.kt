package softagi.ss.hermosauser.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class userModel(
    var firstName: String? = "",
    var lastName: String? = "",
    var email: String? = "",
    var city: String? = "",
    var mobile: String? = "",
    var token: String? = "",
    var uId: String? = ""
)