package softagi.ss.hermosauser.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class tokenModel(
    var token: String? = "",
    var uId: String? = ""
)