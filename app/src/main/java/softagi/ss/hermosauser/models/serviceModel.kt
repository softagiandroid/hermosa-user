package softagi.ss.hermosauser.models

import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable

@IgnoreExtraProperties
class serviceModel(
    var imageUrl: String? = "",
    var titleEn: String? = "",
    var titleAr: String? = "",
    var descEn: String? = "",
    var descAr: String? = "",
    var price: String? = "",
    var homePrice: String? = "",
    var currency: String? = "",
    var duration: String? = "",
    var mainServiceTitle: String? = "",
    var mainServiceId: String? = "",
    var salonName: String? = "",
    var salonId: String? = "",
    var home: Boolean? = false,
    var id: String? = ""
) : Serializable