package softagi.ss.hermosauser.models

import com.google.firebase.database.Exclude

class bookingModel(
    var id: String? = "",
    var uId:String? = "",
    var date:String? = "",
    var list:MutableList<cartModel>
)
{
    @Exclude
    fun toMap(): Map<String, Any?>
    {
        return mapOf(
            "id" to id,
            "uId" to uId,
            "date" to date,
            "list" to list
        )
    }
}