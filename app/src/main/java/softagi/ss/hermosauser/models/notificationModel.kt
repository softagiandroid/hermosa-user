package softagi.ss.hermosauser.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class notificationModel(
    var body: String? = "",
    var title: String? = "",
    var type: Int? = 0
)