package softagi.ss.hermosauser.models

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class cartModel(
    var userModel: userModel? = null,
    var salonModel: salonModel? = null,
    var serviceModel: serviceModel? = null,
    var teamModel: teamModel? = null,
    var id: String? = ""
)
{
    @Exclude
    fun toMap(): Map<String, Any?>
    {
        return mapOf(
            "userModel" to userModel,
            "salonModel" to salonModel,
            "serviceModel" to serviceModel,
            "teamModel" to teamModel,
            "id" to id
            )
    }
}