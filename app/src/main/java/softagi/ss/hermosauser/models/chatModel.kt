package softagi.ss.hermosauser.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class chatModel(
    var message: String? = "",
    var senderId: String? = "",
    var type: Int? = 0
)