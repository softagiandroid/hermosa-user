package softagi.ss.hermosauser.models

import com.google.firebase.database.IgnoreExtraProperties
import java.io.Serializable

@IgnoreExtraProperties
class salonModel(
    var userName: String? = "",
    var salonName: String? = "",
    var email: String? = "",
    var city: String? = "",
    var firstMobile: String? = "",
    var secondMobile: String? = "",
    var thirdMobile: String? = "",
    var salonImage: String? = "",
    var salonLicense: String? = "",
    var firstBank: String? = "",
    var firstBankCard: String? = "",
    var secondBank: String? = "",
    var secondBankCard: String? = "",
    var salonLatitude: String? = "",
    var salonLongitude: String? = "",
    var salonAddress: String? = "",
    var visa: Boolean? = false,
    var deposit: Boolean? = false,
    var currency: String? = "",
    var currencyId: String? = "",
    var uId: String? = ""
):Serializable