package softagi.ss.hermosauser.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class daysModel(
    var day: String? = "",
    var from: String? = "",
    var to: String? = "",
    var status: Boolean? = false
)