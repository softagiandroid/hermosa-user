package softagi.ss.hermosauser.models

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class teamModel(
    var imageUrl: String? = "",
    var NameEn: String? = "",
    var Namear: String? = "",
    var descEn: String? = "",
    var descAr: String? = "",
    var serviceTitle: String? = "",
    var serviceId: String? = "",
    var salonName: String? = "",
    var salonId: String? = "",
    var id: String? = ""
)